import React from "react";
import { BsWhatsapp } from "react-icons/bs";
import { FaTelegramPlane } from "react-icons/fa";
import { Button, Modal } from "react-bootstrap";

export default function ModalOrderTranscript(props) {
  return (
    <Modal show={props.IsShowModal} onHide={(e) => props.handleShowModal(false)} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header closeButton>
        <Modal.Title>
          <span className="text-primary">Доступны описание позиций | арканов | консультации по портрету</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <p>После оплаты предоставляется PDF файл - методичка с подробным описанием выбранных позиций Архетипического Портрета и формулы расчёта по каждой позиции.</p>
          <p>
            Для заказа выберете и введите КОД с номером нужного описания. Если вы хотите приобрести описание всех позиций Полного Портрета и формулы расчёта, напишите{" "}
            <span className="text-primary">
              <b>КОД «+»</b>
            </span>
          </p>
          <p>
            <span className="text-primary">
              <b>КОД #1</b>
            </span>{" "}
            описание расширенного Базового Портрета - позиции <span className="text-primary">1,2,3,4,5,6,7,8,12,13,14</span> + формула расчёта
          </p>
          <p>
            <span className="text-primary">
              <b>КОД #2</b>
            </span>{" "}
            описание Генетического Портрета - позиции <span className="text-primary">9,10,11,15А/В, 16, 17,18 (+19,20)</span> + формула расчёта
          </p>
          <p>
            <span className="text-primary">
              <b>КОД #3</b>
            </span>{" "}
            описание Теневого Портрета - позиции <span className="text-primary">A,B,C,D,E,F,H,G</span> + формула расчёта
          </p>
          <p>
            <span className="text-primary">
              <b>КОД #4</b>
            </span>{" "}
            описание позиций Финансовых Блоков + формула расчёта
          </p>
          <p>
            <span className="text-primary">
              <b>КОД #5</b>
            </span>{" "}
            описание позиций Препятствий в Отношениях + формула расчёта
          </p>
          <p>
            <span className="text-primary">
              <b>КОД #6</b>
            </span>{" "}
            описание позиций Долг перед Родом + формула расчёта
          </p>
          <p className="text-primary">
            Описания позиций дают глубокое понимание, как раскрываются архетипы в зависимости от своего положения в портрете, а так же, как они связаны с другими позициями портрета - всё это
            доступно в описаниях. Актуальную стоимость на методички вы можете узнать, перейдя на «телеграмм» (кнопка) или «вотсап» (кнопка).
          </p>
        </div>
        <hr />
        <div className="d-flex flex-column flex-md-row">
          <div className="d-flex justify-content-center align-items-center flex-grow-1">
            <a href="https://api.whatsapp.com/send/?phone=79122595378" target="_blank" className="btn btn-primary px-3 text-light d-flex justify-content-end align-items-center">
              <BsWhatsapp /> <span className="ms-2">Написать в Whatsapp</span>
            </a>
          </div>
          <div className="d-flex justify-content-center align-items-center flex-grow-1 mt-1 mt-md-0">
            <a href="tg://resolve?domain=LunaChafran" target="_blank" className="btn btn-primary px-3 text-light d-flex justify-content-end align-items-center">
              <FaTelegramPlane /> <span className="ms-2">Написать в Телеграмм</span>
            </a>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" className="text-light" onClick={(e) => props.handleShowModal(false)}>
          Закрыть
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
