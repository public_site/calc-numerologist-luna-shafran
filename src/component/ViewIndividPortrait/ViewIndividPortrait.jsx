import React, { Component, forwardRef } from "react";
import styles from "./ViewIndividPortrait.module.scss";
import CardItem from "../UI/CardItem/CardItem";

/**
 * NameValues
 */
class ViewIndividPortrait extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  returnTrue(value, pos, numbers) {
    console.log([value, pos, numbers]);

    for (var key in numbers) {
      if (key == pos && numbers[key] == value) {
        return true;
      }
    }

    return false;
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var values = this.props.Values;

    var numberSelected = !!this.props.NumberSelected ? this.props.NumberSelected : [];
    var clsContainer = [styles.Container];
    var isTwoPortrait = !!this.props.IsTwoPortrait;
    if (isTwoPortrait) {
      clsContainer.push(styles.ContainerTwoPortrait);
    }

    return (
      <div className={clsContainer.join(" ")}>
        <div className="row">
          <div className={`col-2 text-center ${styles.Col}`}>
            <CardItem
              IsTwoPortrait={isTwoPortrait}
              ValueNumberTitle={`15`}
              ValueNumber={values.ValuePosition15}
              IsVisualTaro={this.props.IsVisualTaro}
              ValuePower={values.ValuePower}
              ValueShadow={values.ValueShadow}
              ValueTarget={values.ValueTarget}
              IsSelected={this.returnTrue(values.ValuePosition15, "ValuePosition15", numberSelected)}
            />
          </div>
          <div className={`col-8 text-center ${styles.Col}`}>
            <CardItem
              IsTwoPortrait={isTwoPortrait}
              ValueNumberTitle={`11`}
              ValueNumber={values.ValuePosition11}
              IsVisualTaro={this.props.IsVisualTaro}
              ValuePower={values.ValuePower}
              ValueShadow={values.ValueShadow}
              ValueTarget={values.ValueTarget}
              IsSelected={this.returnTrue(values.ValuePosition11, "ValuePosition11", numberSelected)}
            />
          </div>
          <div className={`col-2 text-center ${styles.Col}`}>
            <CardItem
              IsTwoPortrait={isTwoPortrait}
              ValueNumberTitle={`18`}
              ValueNumber={values.ValuePosition18}
              IsVisualTaro={this.props.IsVisualTaro}
              ValuePower={values.ValuePower}
              ValueShadow={values.ValueShadow}
              ValueTarget={values.ValueTarget}
              IsSelected={this.returnTrue(values.ValuePosition18, "ValuePosition18", numberSelected)}
            />
          </div>
        </div>
        <div className="row">
          <div className={`col-2 text-center d-flex flex-column ${styles.Col}`}>
            <div className={`${styles.CardMarginTop}`}>
              <CardItem
                IsTwoPortrait={isTwoPortrait}
                ValueNumberTitle={`13`}
                ValueNumber={values.ValuePosition13}
                IsVisualTaro={this.props.IsVisualTaro}
                ValuePower={values.ValuePower}
                ValueShadow={values.ValueShadow}
                ValueTarget={values.ValueTarget}
                IsSelected={this.returnTrue(values.ValuePosition13, "ValuePosition13", numberSelected)}
              />
            </div>
            <div className="d-flex flex-column flex-grow-1 justify-content-between">
              <CardItem IsTwoPortrait={isTwoPortrait} ValueNumberTitle={``} ValueNumber={``} IsVisualTaro={``} IsSelected={false} />
              <CardItem
                IsTwoPortrait={isTwoPortrait}
                ValueNumberTitle={`30`}
                ValueNumber={values.ValuePosition30}
                IsVisualTaro={this.props.IsVisualTaro}
                ValuePower={values.ValuePower}
                ValueShadow={values.ValueShadow}
                ValueTarget={values.ValueTarget}
                IsSelected={this.returnTrue(values.ValuePosition30, "ValuePosition30", numberSelected)}
              />
              <CardItem
                IsTwoPortrait={isTwoPortrait}
                ValueNumberTitle={`16`}
                ValueNumber={values.ValuePosition16}
                IsVisualTaro={this.props.IsVisualTaro}
                ValuePower={values.ValuePower}
                ValueShadow={values.ValueShadow}
                ValueTarget={values.ValueTarget}
                IsSelected={this.returnTrue(values.ValuePosition16, "ValuePosition16", numberSelected)}
              />
            </div>
          </div>
          <div className={`col-8 ${styles.Col}`}>
            <div className={`row ${styles.RowBorderBottom}`}>
              <div className={`col-6 text-center ${styles.Col}`}>
                <CardItem
                  IsTwoPortrait={isTwoPortrait}
                  ValueNumberTitle={`9`}
                  ValueNumber={values.ValuePosition9}
                  IsVisualTaro={this.props.IsVisualTaro}
                  ValuePower={values.ValuePower}
                  ValueShadow={values.ValueShadow}
                  ValueTarget={values.ValueTarget}
                  IsSelected={this.returnTrue(values.ValuePosition9, "ValuePosition9", numberSelected)}
                />
              </div>
              <div className={`col-6 text-center ${styles.Col}`}>
                <CardItem
                  IsTwoPortrait={isTwoPortrait}
                  ValueNumberTitle={`10`}
                  ValueNumber={values.ValuePosition10}
                  IsVisualTaro={this.props.IsVisualTaro}
                  ValuePower={values.ValuePower}
                  ValueShadow={values.ValueShadow}
                  ValueTarget={values.ValueTarget}
                  IsSelected={this.returnTrue(values.ValuePosition10, "ValuePosition10", numberSelected)}
                />
              </div>
            </div>
            <div className="row pt-2">
              <div className={`col-3 text-center d-flex flex-column ${styles.Col}`}>
                <CardItem
                  IsTwoPortrait={isTwoPortrait}
                  ValueNumberTitle={`1`}
                  ValueNumber={values.ValuePosition1}
                  IsVisualTaro={this.props.IsVisualTaro}
                  ValuePower={values.ValuePower}
                  ValueShadow={values.ValueShadow}
                  ValueTarget={values.ValueTarget}
                  IsSelected={this.returnTrue(values.ValuePosition1, "ValuePosition1", numberSelected)}
                />
                <div className={`flex-grow-1 ${styles.CenterLine}`}></div>
                <CardItem
                  IsTwoPortrait={isTwoPortrait}
                  ValueNumberTitle={`D`}
                  ValueNumber={values.ValuePositionD}
                  IsVisualTaro={this.props.IsVisualTaro}
                  ValuePower={values.ValuePower}
                  ValueShadow={values.ValueShadow}
                  ValueTarget={values.ValueTarget}
                  IsSelected={this.returnTrue(values.ValuePositionD, "ValuePositionD", numberSelected)}
                />
              </div>
              <div className={`col-6 ${styles.Col}`}>
                <div className="row">
                  <div className={`col-4 text-center d-flex flex-column justify-content-evenly ${styles.Col} ${styles.CardMarginTop}`}>
                    <CardItem
                      IsTwoPortrait={isTwoPortrait}
                      ValueNumberTitle={`4`}
                      ValueNumber={values.ValuePosition4}
                      IsVisualTaro={this.props.IsVisualTaro}
                      ValuePower={values.ValuePower}
                      ValueShadow={values.ValueShadow}
                      ValueTarget={values.ValueTarget}
                      IsSelected={this.returnTrue(values.ValuePosition4, "ValuePosition4", numberSelected)}
                    />
                    <div className={`flex-grow-1 ${styles.CenterLine}`}></div>
                    <div className={`${styles.CardMarginBottom}`}>
                      <CardItem
                        IsTwoPortrait={isTwoPortrait}
                        ValueNumberTitle={`B`}
                        ValueNumber={values.ValuePositionB}
                        IsVisualTaro={this.props.IsVisualTaro}
                        ValuePower={values.ValuePower}
                        ValueShadow={values.ValueShadow}
                        ValueTarget={values.ValueTarget}
                        IsSelected={this.returnTrue(values.ValuePositionB, "ValuePositionB", numberSelected)}
                      />
                    </div>
                  </div>
                  <div className={`col-4 text-center ${styles.Col}`}>
                    <CardItem
                      IsTwoPortrait={isTwoPortrait}
                      ValueNumberTitle={`2`}
                      ValueNumber={values.ValuePosition2}
                      IsVisualTaro={this.props.IsVisualTaro}
                      ValuePower={values.ValuePower}
                      ValueShadow={values.ValueShadow}
                      ValueTarget={values.ValueTarget}
                      IsSelected={this.returnTrue(values.ValuePosition2, "ValuePosition2", numberSelected)}
                    />
                    <CardItem
                      IsTwoPortrait={isTwoPortrait}
                      ValueNumberTitle={`6`}
                      ValueNumber={values.ValuePosition6}
                      IsVisualTaro={this.props.IsVisualTaro}
                      ValuePower={values.ValuePower}
                      ValueShadow={values.ValueShadow}
                      ValueTarget={values.ValueTarget}
                      IsSelected={this.returnTrue(values.ValuePosition6, "ValuePosition6", numberSelected)}
                    />
                    <div className={`${styles.RowBorderBottom}`}>
                      <CardItem
                        IsTwoPortrait={isTwoPortrait}
                        ValueNumberTitle={`8`}
                        ValueNumber={values.ValuePosition8}
                        IsVisualTaro={this.props.IsVisualTaro}
                        ValuePower={values.ValuePower}
                        ValueShadow={values.ValueShadow}
                        ValueTarget={values.ValueTarget}
                        IsSelected={this.returnTrue(values.ValuePosition8, "ValuePosition8", numberSelected)}
                      />
                    </div>
                    <CardItem
                      IsTwoPortrait={isTwoPortrait}
                      ValueNumberTitle={`A`}
                      ValueNumber={values.ValuePositionA}
                      IsVisualTaro={this.props.IsVisualTaro}
                      ValuePower={values.ValuePower}
                      ValueShadow={values.ValueShadow}
                      ValueTarget={values.ValueTarget}
                      IsSelected={this.returnTrue(values.ValuePositionA, "ValuePositionA", numberSelected)}
                    />
                    <CardItem
                      IsTwoPortrait={isTwoPortrait}
                      ValueNumberTitle={`E`}
                      ValueNumber={values.ValuePositionE}
                      IsVisualTaro={this.props.IsVisualTaro}
                      ValuePower={values.ValuePower}
                      ValueShadow={values.ValueShadow}
                      ValueTarget={values.ValueTarget}
                      IsSelected={this.returnTrue(values.ValuePositionE, "ValuePositionE", numberSelected)}
                    />
                  </div>
                  <div className={`col-4 text-center d-flex flex-column justify-content-evenly ${styles.Col} ${styles.CardMarginTop}`}>
                    <CardItem
                      IsTwoPortrait={isTwoPortrait}
                      ValueNumberTitle={`5`}
                      ValueNumber={values.ValuePosition5}
                      IsVisualTaro={this.props.IsVisualTaro}
                      ValuePower={values.ValuePower}
                      ValueShadow={values.ValueShadow}
                      ValueTarget={values.ValueTarget}
                      IsSelected={this.returnTrue(values.ValuePosition5, "ValuePosition5", numberSelected)}
                    />
                    <div className={`flex-grow-1 ${styles.CenterLine}`}></div>
                    <div className={`${styles.CardMarginBottom}`}>
                      <CardItem
                        IsTwoPortrait={isTwoPortrait}
                        ValueNumberTitle={`C`}
                        ValueNumber={values.ValuePositionC}
                        IsVisualTaro={this.props.IsVisualTaro}
                        ValuePower={values.ValuePower}
                        ValueShadow={values.ValueShadow}
                        ValueTarget={values.ValueTarget}
                        IsSelected={this.returnTrue(values.ValuePositionC, "ValuePositionC", numberSelected)}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className={`col-3 text-center d-flex flex-column ${styles.Col}`}>
                <CardItem
                  IsTwoPortrait={isTwoPortrait}
                  ValueNumberTitle={`3`}
                  ValueNumber={values.ValuePosition3}
                  IsVisualTaro={this.props.IsVisualTaro}
                  ValuePower={values.ValuePower}
                  ValueShadow={values.ValueShadow}
                  ValueTarget={values.ValueTarget}
                  IsSelected={this.returnTrue(values.ValuePosition3, "ValuePosition3", numberSelected)}
                />
                <div className={`flex-grow-1 ${styles.CenterLine}`}></div>
                <CardItem
                  IsTwoPortrait={isTwoPortrait}
                  ValueNumberTitle={`F`}
                  ValueNumber={values.ValuePositionF}
                  IsVisualTaro={this.props.IsVisualTaro}
                  ValuePower={values.ValuePower}
                  ValueShadow={values.ValueShadow}
                  ValueTarget={values.ValueTarget}
                  IsSelected={this.returnTrue(values.ValuePositionF, "ValuePositionF", numberSelected)}
                />
              </div>
            </div>
          </div>
          <div className={`col-2 text-center d-flex flex-column ${styles.Col}`}>
            <div className={`${styles.CardMarginTop}`}>
              <CardItem
                IsTwoPortrait={isTwoPortrait}
                ValueNumberTitle={`14`}
                ValueNumber={values.ValuePosition14}
                IsVisualTaro={this.props.IsVisualTaro}
                ValuePower={values.ValuePower}
                ValueShadow={values.ValueShadow}
                ValueTarget={values.ValueTarget}
                IsSelected={this.returnTrue(values.ValuePosition14, "ValuePosition14", numberSelected)}
              />
            </div>
            <div className="d-flex flex-column flex-grow-1 justify-content-between">
              <CardItem
                IsTwoPortrait={isTwoPortrait}
                ValueNumberTitle={`7`}
                ValueNumber={values.ValuePosition7}
                IsVisualTaro={this.props.IsVisualTaro}
                ValuePower={values.ValuePower}
                ValueShadow={values.ValueShadow}
                ValueTarget={values.ValueTarget}
                IsSelected={this.returnTrue(values.ValuePosition7, "ValuePosition7", numberSelected)}
              />
              <CardItem
                IsTwoPortrait={isTwoPortrait}
                ValueNumberTitle={`12`}
                ValueNumber={values.ValuePosition12}
                IsVisualTaro={this.props.IsVisualTaro}
                ValuePower={values.ValuePower}
                ValueShadow={values.ValueShadow}
                ValueTarget={values.ValueTarget}
                IsSelected={this.returnTrue(values.ValuePosition12, "ValuePosition12", numberSelected)}
              />
              <CardItem
                IsTwoPortrait={isTwoPortrait}
                ValueNumberTitle={`17`}
                ValueNumber={values.ValuePosition17}
                IsVisualTaro={this.props.IsVisualTaro}
                ValuePower={values.ValuePower}
                ValueShadow={values.ValueShadow}
                ValueTarget={values.ValueTarget}
                IsSelected={this.returnTrue(values.ValuePosition17, "ValuePosition17", numberSelected)}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className={`col-12 text-center ${styles.Col}`}>
            <CardItem
              IsTwoPortrait={isTwoPortrait}
              ValueNumberTitle={`H`}
              ValueNumber={values.ValuePositionH}
              IsVisualTaro={this.props.IsVisualTaro}
              ValuePower={values.ValuePower}
              ValueShadow={values.ValueShadow}
              ValueTarget={values.ValueTarget}
              IsSelected={this.returnTrue(values.ValuePositionH, "ValuePositionH", numberSelected)}
            />
          </div>
        </div>
      </div>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default ViewIndividPortrait;
