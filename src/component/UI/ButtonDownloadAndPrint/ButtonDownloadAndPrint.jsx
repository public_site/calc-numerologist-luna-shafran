import React, { Component } from "react";
import styles from "./ButtonDownloadAndPrint.module.scss";
import imgArrowDownGold from "../../../img/arrow_down_gold.png";
import { VscArrowDown } from "react-icons/vsc";
import { CgPushDown } from "react-icons/cg";

class ButtonDownloadAndPrint extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return (
      <button className={`btn btn-link d-flex flex-column align-items-center ${styles.ButtonAnimation}`} onClick={(e) => this.props.handlePrint(e)}>
        <CgPushDown className={styles.TextIcon} />
        <span className="d-flex justify-content-center align-items-center">
          <span className={styles.Text}>скачать</span>
          <span className="mx-1">|</span>
          <span className={styles.Text}>печать</span>
        </span>
      </button>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default ButtonDownloadAndPrint;
