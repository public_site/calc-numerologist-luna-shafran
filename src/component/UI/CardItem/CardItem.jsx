import React, { Component } from "react";
import styles from "./CardItem.module.scss";
import imgItem1 from "../../../img/item1.jpg";
import imgItem2 from "../../../img/item2.jpg";
import imgItem3 from "../../../img/item3.jpg";
import imgItem4 from "../../../img/item4.jpg";
import imgItem5 from "../../../img/item5.jpg";
import imgItem6 from "../../../img/item6.jpg";
import imgItem7 from "../../../img/item7.jpg";
import imgItem8 from "../../../img/item8.jpg";
import imgItem9 from "../../../img/item9.jpg";
import imgItem10 from "../../../img/item10.jpg";
import imgItem11 from "../../../img/item11.jpg";
import imgItem12 from "../../../img/item12.jpg";
import imgItem13 from "../../../img/item13.jpg";
import imgItem14 from "../../../img/item14.jpg";
import imgItem15 from "../../../img/item15.jpg";
import imgItem16 from "../../../img/item16.jpg";
import imgItem17 from "../../../img/item17.jpg";
import imgItem18 from "../../../img/item18.jpg";
import imgItem19 from "../../../img/item19.jpg";
import imgItem20 from "../../../img/item20.jpg";
import imgItem21 from "../../../img/item21.jpg";
import imgItem22 from "../../../img/item22.jpg";
import imgItem30 from "../../../img/item30.jpg";
import CardItemTitle from "../CardItemTitle/CardItemTitle";
import { getDescPositionForIndividPortrait } from "../../../Func";

class CardItem extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var isVisualTaro = this.props.IsVisualTaro;
    var numberPosition = this.props.ValueNumberTitle;
    var valueDesc = this.props.ValueDesc;
    var cls = [styles.CardItem];
    if (!!this.props.IsSelected) {
      cls.push(styles.Selected);
    }

    var isTwoPortrait = !!this.props.IsTwoPortrait;
    if (isTwoPortrait) {
      cls.push(styles.CardItemTwoPortrait);
    }

    return (
      <React.Fragment>
        <div className={cls.join(" ")}>
          <CardItemTitle ValueNumber={numberPosition} ValueDesc={valueDesc} IsTwoPortrait={isTwoPortrait} />
          <div className="d-flex">
            {!isVisualTaro ? (
              <this.renderText
                ValuePower={this.props.ValuePower}
                ValueShadow={this.props.ValueShadow}
                ValueTarget={this.props.ValueTarget}
                ValueNumber={this.props.ValueNumber}
                ValueSup={this.props.ValueSup}
              />
            ) : (
              <this.renderTaro ValueNumber={this.props.ValueNumber} />
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }

  renderText = (props) => {
    var number = props.ValueNumber;
    var sup = props.ValueSup;

    var cls = [styles.TextValue];

    var crossing = null;

    if (!!this.props.ValuePower) {
      if (this.props.ValuePower.indexOf(this.props.ValueNumber) != -1) {
        crossing = "c";
      }
    }
    if (!!this.props.ValueShadow) {
      if (this.props.ValueShadow.indexOf(this.props.ValueNumber) != -1) {
        crossing = "т";
      }
    }
    if (!!this.props.ValueTarget) {
      if (this.props.ValueTarget.indexOf(this.props.ValueNumber) != -1) {
        crossing = "ц";
      }
    }

    return (
      <React.Fragment>
        <span className={cls.join(" ")}>{number}</span>
        {!!sup ? <span className={styles.TextSup}>{sup}</span> : null}
        {!!crossing ? <span className={styles.TextSup}>{crossing}</span> : null}
      </React.Fragment>
    );
  };

  renderTaro = (props) => {
    var number = props.ValueNumber;
    switch (number) {
      case "I":
        return <img src={imgItem1} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "II":
        return <img src={imgItem2} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "III":
        return <img src={imgItem3} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "IV":
        return <img src={imgItem4} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "V":
        return <img src={imgItem5} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "VI":
        return <img src={imgItem6} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "VII":
        return <img src={imgItem7} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "VIII":
        return <img src={imgItem8} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "IX":
        return <img src={imgItem9} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "X":
        return <img src={imgItem10} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XI":
        return <img src={imgItem11} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XII":
        return <img src={imgItem12} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XIII":
        return <img src={imgItem13} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XIV":
        return <img src={imgItem14} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XV":
        return <img src={imgItem15} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XVI":
        return <img src={imgItem16} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XVII":
        return <img src={imgItem17} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XVIII":
        return <img src={imgItem18} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XIX":
        return <img src={imgItem19} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XX":
        return <img src={imgItem20} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XXI":
        return <img src={imgItem21} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      case "XXII":
        return <img src={imgItem22} alt={`Карта Таро${number}`} className={styles.ImgTaro} />;
      default:
        break;
    }
  };

  getDescNumber(number) {
    switch (number) {
      case "I":
        return "Предназначение. Внутренний ребёнок. «Багаж, с которым пришли».";
      case "II":
        return "Урок психологического взросления. Внутренний взрослый. Среда взросления.";
      case "III":
        return "Социальный экзамен. Долг перед социумом. События в +/- зависит сдан ли Экзамен.";
      case "IV":
        return "Деструктивные программы поведения и негативные события. Блоки. Точки боли.";
      case "V":
        return "Соц-е устремления, интересы кем надо стать, а не казаться чтобы быть ценным.";
      case "VI":
        return "Жизненный сценарий. Навыки способности которые отрицаются в себе, а в других нрав.";
      case "VII":
        return "Социальная миссия. При раскрытой 18+ пройденных испытаний в социуме на 8 позиц.";
      case "VIII":
        return "Инструмент миссии. Дар. Если пройдены испытания в социуме - желаемые результаты.";
      case "XI":
        return "Генетический опыт и качества, которые транслируем другим бессознательно.";
      case "X":
        return "Генетические цели, с чем связана активность устремления, действия человека в социуме.";
      case "XI":
        return "Генетическая Миссия. Смысл жизни на ген. ур. Раскрыть 15п. + препятствия 17п. = талант в +";
      case "XII":
        return "Условия Психологич. комфорта в социуме. Среда для продуктивной реализации в соц.";
      case "XIII":
        return "Самооценка, каким видим себя, окр. мир. Переживания, страхи, неуверенность в себе.";
      case "XIV":
        return "Впечатления, которые производим в социуме. Каким нас считывают окружающие, среда.";
      case "XV":
        return "Встроенные внутриличностный таланты и навыки для успешной реализации генетической Миссии п.11";
      case "XVI":
        return "Ошибки, тормозящие успех, создающие проблемы в жизни и реализации. Грабли.";
      case "XVII":
        return "Комфортная соцсреда, где есть генет. опыт + деструктивные психол. программы привод в -";
      case "XVIII":
        return "Встроенные соц таланты и способности для успешной реализации миссии в социуме по 7п.";
      case "XIX":
        return "Описание позиции 19 ...";
      case "XX":
        return "Теневой внутренний ребенок. Комплексы и Ресурс, выход во взрослый сценарий.";
      case "XXI":
        return "Область борьбы, что сопровождает всегда и влияние травмы на отнош. и реализацию.";
      case "XXII":
        return "Способности в социуме которые недооценены, в чем можем преуспеть + Демонстр. поведение.";
      case "XXIII":
        return "Стрессовое поведение, что тяжело дается, но это помогает справляться с трудностями в социуме, финансах, заработанных собственным трудом.";
      case "XXIV":
        return "Среда и условия, предохраняющие от стрессов, помогают восстановиться. В избытке приводит к регрессу и блоку в развитии. Талант и страж, переходящий в огранич. убеждения.";
      case "XXV":
        return "Не чувствуем это в себе, но нравится в других. Осознав это ресурс в себе и начав им пользоваться - обретаем большую внутреннюю силу. Отвержденная часть психики.";
      case "XXVI":
        return "Деструктивные привычки, в неформальном общении приводящие к проблемам в отношениях вне социума.";
      default:
        return "Описание отсутствует, но оно скоро тут появится.";
    }
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }

  getDesc(number) {
    switch (number) {
      case "I":
        return "";
      case "II":
        return "";
      case "III":
        return "";
      case "IV":
        return "";
      case "V":
        return "";
      case "VI":
        return "";
      case "VII":
        return "";
      case "VIII":
        return "";
      case "IX":
        return "";
      case "X":
        return "";
      case "XI":
        return "";
      case "XII":
        return "";
      case "XIII":
        return "";
      case "XIV":
        return "";
      case "XV":
        return "";
      case "XVI":
        return "";
      case "XVII":
        return "";
      case "XVIII":
        return "";
      case "XIX":
        return "";
      case "XX":
        return "";
      case "XXI":
        return "";
      case "XXII":
        return "";
      case "XXIII":
        return "";
      default:
        return "Описание отсутствует, но оно скоро тут появится.";
    }
  }
}

export default CardItem;
