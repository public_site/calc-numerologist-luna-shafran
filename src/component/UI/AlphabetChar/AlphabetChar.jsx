import React, { Component } from "react";
import styles from "./AlphabetChar.module.scss";

const AlphabetChar = (props) => {
  var cls = ["container", styles.Container];
  return (
    <React.Fragment>
      <div className={styles.ItemChar}>
        <div className={styles.ItemCharValue}>{props.ValueNumberPosValue}</div>
        <div className={styles.ItemCharNumber}>{props.ValueNumberPosNumber}</div>
      </div>
    </React.Fragment>
  );
};

export default AlphabetChar;
