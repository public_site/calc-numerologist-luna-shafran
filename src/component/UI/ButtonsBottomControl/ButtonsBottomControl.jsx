import React, { Component } from "react";
import styles from "./ButtonsBottomControl.module.scss";
import imgArrowLeftBlack from "../../../img/arrow_left_black.png";
import imgArrowRightBlack from "../../../img/arrow_right_black.png";
import { Link } from "react-router-dom";
import { BsWhatsapp } from "react-icons/bs";
import { FaTelegramPlane } from "react-icons/fa";
import { Button, Modal } from "react-bootstrap";
import ModalOrderTranscript from "../../ModalOrderTranscript/ModalOrderTranscript";

class ButtonsBottomControl extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      IsShowModal: false,
    };
  }

  handleShowModal = (isModalShow) => {
    this.setState({
      IsShowModal: isModalShow,
    });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var buttonLeftText = !!this.props.ButtonLeftText ? this.props.ButtonLeftText : "назад к вводу даты";
    var buttonCenter = !!this.props.ButtonCenter ? this.props.ButtonCenter : "подробности к портрету";
    var buttonRight = !!this.props.ButtonRight ? this.props.ButtonRight : "композитный портрет";

    return (
      <React.Fragment>
        <div className={`row my-3 ${styles.PositionMobile}`}>
          <div className="col-12 col-md-4 d-flex justify-content-md-end justify-content-center align-items-center">
            <div className="d-flex justify-content-center align-items-center">
              <button className="btn btn-link text-decoration-none" onClick={(e) => this.props.onButtonLeft(e)}>
                <span className="me-1">{buttonLeftText}</span>
              </button>
              <img src={imgArrowLeftBlack} alt="<" className={styles.ImgIcon} />
            </div>
          </div>
          <div className="col-12 col-md-4 text-center d-flex align-items-center">
            <button className="btn btn-primary px-3 w-100 text-light d-flex justify-content-end align-items-center" onClick={(e) => this.handleShowModal(true)}>
              <BsWhatsapp />
              <span className="flex-grow-1">{buttonCenter}</span>
              <span className={styles.BorderCircle}>
                <FaTelegramPlane />
              </span>
            </button>
          </div>
          <div className="col-12 col-md-4 d-flex justify-content-md-start justify-content-center align-items-center">
            <div className="d-flex justify-content-center align-items-center">
              {!this.props.ButtonRightUrl ? (
                <React.Fragment>
                  <img src={imgArrowRightBlack} alt=">" />
                  <button className="btn btn-link text-decoration-none" onClick={(e) => this.props.onButtonRight(e)}>
                    <span className="ms-1">{buttonRight}</span>
                  </button>
                </React.Fragment>
              ) : (
                <Link to={this.props.ButtonRightUrl}>
                  <img src={imgArrowRightBlack} alt=">" />
                  <button className="btn btn-link text-decoration-none">
                    <span className="ms-1">{buttonRight}</span>
                  </button>
                </Link>
              )}
            </div>
          </div>
        </div>

        <ModalOrderTranscript IsShowModal={this.state.IsShowModal} handleShowModal={this.handleShowModal} />
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default ButtonsBottomControl;
