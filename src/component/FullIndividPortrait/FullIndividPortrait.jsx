import React, { Component } from "react";
import styles from "./FullIndividPortrait.module.scss";
import { CardPortrait } from "../UI/CardPortrait/CardPortrait";
import ViewIndividPortrait from "../ViewIndividPortrait/ViewIndividPortrait";
import ButtonsBottomControl from "../UI/ButtonsBottomControl/ButtonsBottomControl";
import imgArrowDownGold from "../../img/arrow_down_gold.png";
import TargetPowerShadow from "../UI/TargetPowerShadow/TargetPowerShadow";
import ButtonDownloadAndPrint from "../UI/ButtonDownloadAndPrint/ButtonDownloadAndPrint";
import * as htmlToImage from "html-to-image";
import { jsPDF } from "jspdf";
import uuid from "react-uuid";

class FullIndividPortrait extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      IdCardPortrait: uuid(),
      IsPrint: false,
      PeriodStartCircle1: "0",
      PeriodEndCircle1: "",
      PeriodStartCircle2: "",
      PeriodEndCircle2: "",
      PeriodStartCircle3: "",
      PeriodEndCircle3: "...",
      values: {},
    };
  }

  /**
   * Скачать и печать
   * @param {*} e Event
   */
  handlePrint = (e) => {
    if (this.state.IsPrint) {
      return;
    }
    this.setState({
      IsPrint: true,
    });
    var node = document.getElementById(this.state.IdCardPortrait);
    htmlToImage
      .toPng(node)
      .then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;

        const doc = new jsPDF();
        doc.addImage(dataUrl, "JPEG", 15, 15, 180, 216);
        doc.save("full_individ_portrait.pdf");
      })
      .catch(function (error) {
        console.error("oops, something went wrong!", error);
      });

    if (!this.state.IsPrint) {
      setTimeout(() => {
        this.setState({
          IsPrint: false,
        });
      }, 1500);
    }
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var values = this.state.values;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12 text-center">
            <h1 className="h3 text-uppercase my-3">Индивидуальный Портрет | PRO</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <CardPortrait Id={this.state.IdCardPortrait} IsPrint={this.state.IsPrint} handlePrint={this.handlePrint} PortraitName="FullIndivid">
              <div className="row font-cambria">
                <div className="col-12 d-flex flex-column justify-content-center align-items-center">
                  <div className="d-flex justify-content-center align-items-center">
                    <span className="h4 m-0 text-capitalize">{this.props.Name}</span>
                  </div>
                  <div className="d-flex justify-content-center align-items-center">
                    <span>Архетип имени: </span> <span className={`text-primary ms-1 ${styles.TextNameArchetype}`}>{this.props.NameArchetype}</span>
                  </div>
                  <div className="">
                    <TargetPowerShadow ValueTarget={values.ValueTarget} ValuePower={values.ValuePower} ValueShadow={values.ValueShadow} />
                  </div>
                  <div className="d-flex flex-column flex-md-row justify-content-center align-items-center font-cambria">
                    <div className="h4 m-0 my-xl-3">
                      <span className="me-3 me-md-3">Дата:</span>
                      <span>{this.props.DateOfBirth}</span>
                    </div>
                  </div>
                </div>
                <div className="col-12 col-md-2 "></div>
              </div>
              <div className="row font-cambria">
                <div className="col-4 text-center">
                  <div className={styles.TitleCycle}>цикл №1</div>
                  <div className="d-flex justify-content-center align-items-center">
                    <span className="px-1">|</span>
                    <span>{this.state.PeriodStartCircle1}</span>
                    <span>~</span>
                    <span>{this.state.PeriodEndCircle1}</span>
                    <span className="ps-1">лет</span>
                    <span className="px-1">|</span>
                  </div>
                </div>
                <div className="col-4 text-center">
                  <div className={styles.TitleCycle}>цикл №2</div>
                  <div className="d-flex justify-content-center align-items-center">
                    <span className="px-1">|</span>
                    <span>{this.state.PeriodStartCircle2}</span>
                    <span>~</span>
                    <span>{this.state.PeriodEndCircle2}</span>
                    <span className="ps-1">лет</span>
                    <span className="px-1">|</span>
                  </div>
                </div>
                <div className="col-4 text-center">
                  <div className={styles.TitleCycle}>цикл №3</div>
                  <div className="d-flex justify-content-center align-items-center">
                    <span className="px-1">|</span>
                    <span>{this.state.PeriodStartCircle3}</span>
                    <span>~</span>
                    <span>{this.state.PeriodEndCircle3}</span>
                    <span className="ps-1">лет</span>
                    <span className="px-1">|</span>
                  </div>
                </div>
              </div>
              <hr className="my-2" />
              <div className="row">
                <div className="col-12">
                  <ViewIndividPortrait Values={values} IsVisualTaro={this.props.IsVisualTaro} handlePrint={this.handlePrint} />
                  {!this.state.IsPrint ? (
                    <div className={styles.ButtonPrint}>
                      <ButtonDownloadAndPrint handlePrint={this.handlePrint} />
                    </div>
                  ) : null}
                </div>
              </div>
            </CardPortrait>
          </div>
        </div>
        <ButtonsBottomControl
          ButtonLeftText="назад к вводу даты"
          ButtonCenter="подробности к портрету"
          ButtonRight="композитный портрет"
          onButtonCenter={(e) => this.handleShowModal1(true)}
          ButtonRightUrl={"/calc-numerologist-luna-shafran/composite-portrait"}
          onButtonLeft={(e) => this.props.onChangeAction("Form")}
        />
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    var dtString = this.props.DateOfBirth + "";
    var periodEndCircle1 = 0;
    for (let i = 0; i < dtString.length; i++) {
      var parsed = parseInt(dtString[i]);
      if (!isNaN(parsed)) {
        periodEndCircle1 += parsed;
      }
    }
    var periodStartCircle2 = periodEndCircle1 + 1;
    var periodEndCircle2 = periodStartCircle2 + 8;
    var periodStartCircle3 = periodEndCircle2 + 1;
    this.setState({
      PeriodEndCircle1: periodEndCircle1,
      PeriodStartCircle2: periodStartCircle2,
      PeriodEndCircle2: periodEndCircle2,
      PeriodStartCircle3: periodStartCircle3,
      values: this.props.Values,
    });
  }
}

export default FullIndividPortrait;
