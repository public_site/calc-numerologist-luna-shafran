import React, { Component } from "react";
import styles from "./AlphabetPortraitAnalysis2.module.scss";
import AlphabetChar from "../UI/AlphabetChar/AlphabetChar";
import { CardPortrait } from "../UI/CardPortrait/CardPortrait";
import ViewIndividPortrait from "../ViewIndividPortrait/ViewIndividPortrait";
import ButtonsBottomControl from "../UI/ButtonsBottomControl/ButtonsBottomControl";
import ButtonDownloadAndPrint from "../UI/ButtonDownloadAndPrint/ButtonDownloadAndPrint";
import * as htmlToImage from "html-to-image";
import { jsPDF } from "jspdf";
import uuid from "react-uuid";

class AlphabetPortraitAnalysis2 extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      IdCardPortrait: uuid(),
      IsPrint: false,
    };
  }

  handlePrint = (e) => {
    if (this.state.IsPrint) {
      return;
    }
    this.setState({
      IsPrint: true,
    });
    var node = document.getElementById(this.state.IdCardPortrait);
    htmlToImage
      .toPng(node)
      .then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;

        const doc = new jsPDF();
        doc.addImage(dataUrl, "JPEG", 15, 15, 180, 216);
        doc.save("alphabet.pdf");
      })
      .catch(function (error) {
        console.error("oops, something went wrong!", error);
      });

    if (!this.state.IsPrint) {
      setTimeout(() => {
        this.setState({
          IsPrint: false,
        });
      }, 1500);
    }
  };
  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var values = this.props.Values;
    var dataForm = this.props.DataForm;
    return (
      <React.Fragment>
        <hr />
        <CardPortrait Id={this.state.IdCardPortrait} IsPrint={this.state.IsPrint} handlePrint={(e) => this.props.onChangeAction("AlphabetPortrait")} PortraitName="Alphabet">
          <div className="row">
            <div className="col-12 d-flex flex-column justify-content-center align-items-center">
              <div className="d-flex justify-content-center align-items-center">
                <h3 className="h3">АЛФАВИТНЫЙ ПОРТРЕТ</h3>
              </div>
              <div className="d-flex flex-column">
                {this.props.NameValues.map((nameValue, indexJ) => {
                  return (
                    <React.Fragment key={indexJ}>
                      <div className="mb-3">
                        <div className="d-flex flex-column">
                          <div className="d-flex justify-content-center align-items-center text-uppercase font-KabelBookTT">{nameValue.ValueName}</div>
                          <div className="d-flex justify-content-center align-items-center">
                            {nameValue.ValuesNumberPos.map((valueNumberPos, index) => {
                              return <AlphabetChar key={index} ValueNumberPosValue={valueNumberPos[0]} ValueNumberPosNumber={valueNumberPos[1]} />;
                            })}
                          </div>
                        </div>
                        <div className="d-flex justify-content-center align-items-center">
                          <div className="d-flex justify-content-center align-items-center">
                            <div className="me-2">Цель</div>
                            <div className="text-primary">
                              <span className="h5 m-0">
                                <b>{nameValue.ValueTarget}</b>
                              </span>
                            </div>
                          </div>
                          <div className="d-flex justify-content-center align-items-center mx-3">
                            <div className="me-2">Сила</div>
                            <div className="text-primary">
                              <span className="h5 m-0">
                                <b>{nameValue.ValuePower}</b>
                              </span>
                            </div>
                          </div>
                          <div className="d-flex justify-content-center align-items-center">
                            <div className="me-2">Тень</div>
                            <div className="text-primary">
                              <span className="h5 m-0">
                                <b>{nameValue.ValueShadow}</b>
                              </span>
                            </div>
                          </div>
                          <div className="d-flex justify-content-center align-items-center ms-3">
                            <div className="me-2">Главный аркан</div>
                            <div className="text-primary">
                              <span className="h5 m-0">
                                <b>{nameValue.MainArcane}</b>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </React.Fragment>
                  );
                })}
              </div>
              <div className="my-2">
                <h4 className="h5 m-0">Анализируем совпадение в портрете</h4>
              </div>
            </div>
            <div className="col-12 col-md-2"></div>
          </div>

          <div className="row">
            <div className="col-12">
              <div className="d-flex flex-column flex-md-row justify-content-center align-items-center">
                <div className="h4 m-0 my-xl-3">
                  <span className="me-3 me-md-3">Дата:</span>
                  <span>{dataForm.InputDate.Value}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <ViewIndividPortrait Values={values} handlePrint={this.handlePrint} />
            </div>
          </div>
          {!this.state.IsPrint ? (
            <div className={`${styles.ButtonPrint} hidden-print`}>
              <ButtonDownloadAndPrint handlePrint={this.handlePrint} />
            </div>
          ) : null}
        </CardPortrait>
        <ButtonsBottomControl
          ButtonLeftText="назад к вводу даты"
          ButtonCenter="подробности к портрету"
          ButtonRight="композитный портрет"
          ButtonRightUrl={"/calc-numerologist-luna-shafran/composite-portrait"}
          onButtonLeft={(e) => {
            this.props.onChangeAction("AlphabetPortrait");
          }}
        />
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    var isPrint = this.props.IsPrint;
    this.setState({
      IsPrint: isPrint,
    });
  }
}

export default AlphabetPortraitAnalysis2;
