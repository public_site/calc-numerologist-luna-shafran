import React, { Component } from "react";
import styles from "./CommmonPortrait.module.scss";
import AlphabetChar from "../UI/AlphabetChar/AlphabetChar";
import { CardPortrait } from "../UI/CardPortrait/CardPortrait";
import ViewIndividPortrait from "../ViewIndividPortrait/ViewIndividPortrait";
import ButtonDownloadAndPrint from "../UI/ButtonDownloadAndPrint/ButtonDownloadAndPrint";
import * as htmlToImage from "html-to-image";
import { jsPDF } from "jspdf";
import uuid from "react-uuid";

class CommmonPortrait extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      IdCardPortrait: uuid(),
      IsPrint: false,
    };
  }

  /**
   * Скачать и печать
   * @param {*} e Event
   */
  handlePrint = (e) => {
    if (this.state.IsPrint) {
      return;
    }
    this.setState({
      IsPrint: true,
    });
    var node = document.getElementById(this.state.IdCardPortrait);
    htmlToImage
      .toPng(node)
      .then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;

        const doc = new jsPDF();
        doc.addImage(dataUrl, "JPEG", 180, 110, 250, 163, 0, 0, 90);
        doc.save("two_portrait.pdf");
      })
      .catch(function (error) {
        console.error("oops, something went wrong!", error);
      });

    if (!this.state.IsPrint) {
      setTimeout(() => {
        this.setState({
          IsPrint: false,
        });
      }, 1500);
    }
  };
  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var dataForm = this.props.DataForm;
    var values1 = this.props.Values.Values1;
    var values2 = this.props.Values.Values2;
    var values3 = this.props.Values.Values3;
    return (
      <CardPortrait Id={this.state.IdCardPortrait} IsPrint={this.state.IsPrint} handlePrint={(e) => this.props.onChangeAction("Form")} PortraitName="TwoPortrait">
        <div className="row">
          <div className="col-4">
            <div className="d-flex justify-content-center align-items-center">
              <h4 className="h4">
                <b>Портрет №1</b>
              </h4>
            </div>
            <div className="d-flex justify-content-center align-items-center">
              <div className="d-flex justify-content-center align-items-center">
                {values1.ValuesNumberPos.map((valueNumberPos, index) => {
                  return <AlphabetChar key={index} ValueNumberPosValue={valueNumberPos[0]} ValueNumberPosNumber={valueNumberPos[1]} />;
                })}
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center w-100">
              <div className="row my-3">
                <div className="col-12 d-flex justify-content-center align-items-center">
                  <div className="d-flex justify-content-center align-items-center">
                    <div className="me-2">Цель</div>
                    <div className="text-primary">
                      <span className="h5 m-0">
                        <b>{values1.ValueTarget}</b>
                      </span>
                    </div>
                  </div>
                  <div className="d-flex justify-content-center align-items-center mx-3">
                    <div className="me-2">Сила</div>
                    <div className="text-primary">
                      <span className="h5 m-0">
                        <b>{values1.ValuePower}</b>
                      </span>
                    </div>
                  </div>
                  <div className="d-flex justify-content-center align-items-center">
                    <div className="me-2">Тень</div>
                    <div className="text-primary">
                      <span className="h5 m-0">
                        <b>{values1.ValueShadow}</b>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center">
              <span className="mt-1 mb-3">Анализируем совпадение в портрете</span>
            </div>
            <div className="d-flex flex-column flex-md-row justify-content-center align-items-center">
              <div className="h5 m-0 mb-2">
                <b>
                  <span className="me-3 me-md-3">Дата:</span>
                  <span>{dataForm.InputDate1.Value}</span>
                </b>
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center">
              <this.renderPeriod Values={this.props.Values.Period1} />
            </div>
          </div>
          <div className="col-4 d-flex flex-column justify-content-end">
            <div className="d-flex justify-content-center align-items-center">
              <h4 className="h4 text-uppercase">
                <b>Портрет совместимости</b>
              </h4>
            </div>
            <div className="d-flex justify-content-center align-items-center h5 text-primary">
              <div>
                <b>{dataForm.InputDate1.Value}</b>
              </div>
              <div>
                <b>+</b>
              </div>
              <div>
                <b>{dataForm.InputDate2.Value}</b>
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center h5 text-primary text-decoration-underline mb-3">
              <b>Разбор отношений</b>
            </div>
          </div>
          <div className="col-4">
            <div className="d-flex justify-content-center align-items-center">
              <h4 className="h4">
                <b>Портрет №2</b>
              </h4>
            </div>
            <div className="d-flex justify-content-center align-items-center">
              <div className="d-flex justify-content-center align-items-center">
                {values2.ValuesNumberPos.map((valueNumberPos, index) => {
                  return <AlphabetChar key={index} ValueNumberPosValue={valueNumberPos[0]} ValueNumberPosNumber={valueNumberPos[1]} />;
                })}
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center w-100">
              <div className="row my-3">
                <div className="col-12 d-flex justify-content-center align-items-center">
                  <div className="d-flex justify-content-center align-items-center">
                    <div className="me-2">Цель</div>
                    <div className="text-primary">
                      <span className="h5 m-0">
                        <b>{values2.ValueTarget}</b>
                      </span>
                    </div>
                  </div>
                  <div className="d-flex justify-content-center align-items-center mx-3">
                    <div className="me-2">Сила</div>
                    <div className="text-primary">
                      <span className="h5 m-0">
                        <b>{values2.ValuePower}</b>
                      </span>
                    </div>
                  </div>
                  <div className="d-flex justify-content-center align-items-center">
                    <div className="me-2">Тень</div>
                    <div className="text-primary">
                      <span className="h5 m-0">
                        <b>{values2.ValueShadow}</b>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center">
              <span className="mt-1 mb-3">Анализируем совпадение в портрете</span>
            </div>
            <div className="d-flex flex-column flex-md-row justify-content-center align-items-center">
              <div className="h5 m-0 mb-2">
                <b>
                  <span className="me-3 me-md-3">Дата:</span>
                  <span>{dataForm.InputDate2.Value}</span>
                </b>
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center">
              <this.renderPeriod Values={this.props.Values.Period2} />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-4">
            <div className="w-100">
              <ViewIndividPortrait NumberSelected={this.props.NumberSelected} Values={values1} IsTwoPortrait={true} />
            </div>
          </div>
          <div className="col-4">
            <div className="w-100">
              <ViewIndividPortrait NumberSelected={this.props.NumberSelected} Values={values3} IsTwoPortrait={true} />
            </div>
          </div>
          <div className="col-4">
            <div className="w-100">
              <ViewIndividPortrait NumberSelected={this.props.NumberSelected} Values={values2} IsTwoPortrait={true} />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-4">
            <p className="text-start">
              Негативные паттерны поведения <b>Партнера I</b> <br />
              <span className="me-1 text-primary">
                <b>
                  {values1.ValuePositionA} - {values1.ValuePosition12} - {values3.NegativeIndividPortrait1}
                </b>
              </span>
              <b>- показывает, чем человека доставляет дискомфорт Партнёру и себе самому в отношениях</b>
            </p>
          </div>
          <div className="col-4">
            <p className="text-center">
              Негативные паттерны поведения <b>Обоих Партнёров</b> <br />
              <span className="me-1 text-primary">
                <b>
                  {values3.ValuePositionA} - {values3.ValuePosition12} - {values3.NegativeTwoPortrait}
                </b>
              </span>
              <b>- смотрим, в чём кроется проблема в отношениях между двумя Партнёрами.</b> <br />
              <b>Детский сценарий</b>{" "}
              <span className="text-primary">
                <b>
                  {values3.ValuePosition4}-{values3.ValuePositionB}
                </b>
              </span>
              . <b>Взрослый сценарий</b>{" "}
              <span className="text-primary">
                <b>
                  {" "}
                  {values3.ValuePosition5}-{values3.ValuePositionC}
                </b>
              </span>
              .
            </p>
          </div>
          <div className="col-4">
            <p className="text-end">
              Негативные паттерны поведения <b>Партнёра 2</b> <br />{" "}
              <span className="me-1 text-primary">
                <b>
                  {values2.ValuePositionA} - {values2.ValuePosition12} - {values3.NegativeIndividPortrait2}
                </b>
              </span>
              <b>- показывают, в чем человек доставляет дискомфорт партнёру и себе самому в отношениях.</b>
            </p>
          </div>
        </div>
        <div className={`${styles.ButtonCancel} hidden-print`}>
          <button className="btn btn-primary d-flex flex-column align-items-center text-light btn-w-150" onClick={(e) => this.props.onChangeAction("Form")}>
            Назад
          </button>
        </div>
        {!this.state.IsPrint ? (
          <div className={`${styles.ButtonPrint} hidden-print`}>
            <ButtonDownloadAndPrint handlePrint={this.handlePrint} />
          </div>
        ) : null}
      </CardPortrait>
    );
  }

  renderPeriod(props) {
    var values = props.Values;
    return (
      <div className="d-flex flex-column mb-3">
        <div className="row font-cambria text-light border-bottom-light">
          <div className="col-4 text-center">
            <div className="d-flex justify-content-center align-items-center">
              <span>{!!values.PeriodStartCircle1 ? values.PeriodStartCircle1 : "..."}</span>
              <span>-</span>
              <span>{values.PeriodEndCircle1}</span>
              <span className="ps-1">лет</span>
            </div>
          </div>
          <div className="col-4 text-center">
            <div className="d-flex justify-content-center align-items-center">
              <span>{values.PeriodStartCircle2}</span>
              <span>-</span>
              <span>{values.PeriodEndCircle2}</span>
              <span className="ps-1">лет</span>
            </div>
          </div>
          <div className="col-4 text-center">
            <div className="d-flex justify-content-center align-items-center">
              <span>{values.PeriodStartCircle3}</span>
              <span>-</span>
              <span>{!!values.PeriodEndCircle3 ? values.PeriodEndCircle3 : "..."}</span>
              <span className="ps-1">лет</span>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center align-items-center">
          <b>периоды жизни</b>
        </div>
      </div>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default CommmonPortrait;
