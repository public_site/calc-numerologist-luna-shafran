import React, { Component } from "react";
import styles from "./CommmonPortraitForm.module.scss";
import { CardPortrait } from "../../UI/CardPortrait/CardPortrait";
import AlphabetChar from "../../UI/AlphabetChar/AlphabetChar";
import InputMask from "react-input-mask";
import imgArrowDownGold from "../../../img/arrow_down_gold.png";
import ButtonDownloadAndPrint from "../../UI/ButtonDownloadAndPrint/ButtonDownloadAndPrint";
import imgArrowLeftBlack from "../../../img/arrow_left_black.png";
import imgArrowRightBlack from "../../../img/arrow_right_black.png";
import { Link } from "react-router-dom";

class CommmonPortraitForm extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var dataForm = this.props.DataForm;

    var isSuccess = !!this.props.IsSuccess;
    var values = this.props.Values;

    var name1 = dataForm.InputName1.Value;
    var date1 = dataForm.InputDate1.Value;
    var clsInputDate1 = ["form-control", styles.InputForm];
    var clsInputName1 = ["form-control", styles.InputForm];
    if (!dataForm.InputDate1.Validate || !dataForm.InputName1.Validate) {
      if (!dataForm.InputDate1.Validate) {
        clsInputDate1.push("is-invalid");
      }
      if (!dataForm.InputName1.Validate) {
        clsInputName1.push("is-invalid");
      }
    }

    var name2 = dataForm.InputName2.Value;
    var date2 = dataForm.InputDate2.Value;
    var clsInputDate2 = ["form-control", styles.InputForm];
    var clsInputName2 = ["form-control", styles.InputForm];
    if (!dataForm.InputDate2.Validate || !dataForm.InputName2.Validate) {
      if (!dataForm.InputDate2.Validate) {
        clsInputDate2.push("is-invalid");
      }
      if (!dataForm.InputName2.Validate) {
        clsInputName2.push("is-invalid");
      }
    }
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <CardPortrait IsBackgroundColor={true}>
              <div className="row">
                <div className="col-12 col-md-6">
                  <div className="row">
                    <div className="col-12">
                      <div className="d-flex justify-content-center align-items-center">
                        <label className="d-flex flex-column justify-content-center align-items-center">
                          <span className={styles.LabelText}>Имя | Наименование | Слово</span>
                          <input type="text" className={clsInputName1.join(" ")} value={name1} onChange={(e) => this.props.onChangeName1(e)} />
                          <span className={`d-flex flex-column justify-content-center align-items-center mb-2 ${styles.LabelText}`}>
                            <span>Ввод возможен на русском</span>
                            <span>латинице | +цифры</span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  {isSuccess ? (
                    <div className="row">
                      <div className="col-12">
                        <div className="d-flex justify-content-center align-items-center">
                          {values.Values1.ValuesNumberPos.map((valueNumberPos, index) => {
                            return <AlphabetChar key={index} ValueNumberPosValue={valueNumberPos[0]} ValueNumberPosNumber={valueNumberPos[1]} />;
                          })}
                        </div>
                      </div>
                      <div className="col-12">
                        <div className={`my-3 ${styles.ItemResContainer}`}>
                          <div className={styles.ItemRes}>
                            <div className={styles.ItemResTitle}>Цель</div>
                            <div className={styles.ItemResValue}>{values.Values1.ValueTarget}</div>
                          </div>
                          <div className={styles.ItemRes}>
                            <div className={styles.ItemResTitle}>Сила</div>
                            <div className={styles.ItemResValue}>{values.Values1.ValuePower}</div>
                          </div>
                          <div className={styles.ItemRes}>
                            <div className={styles.ItemResTitle}>Тень</div>
                            <div className={styles.ItemResValue}>{values.Values1.ValueShadow}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : null}
                  <div className="row">
                    <div className="col-12">
                      <div className="d-flex justify-content-center align-items-center">
                        <label className="d-flex flex-column justify-content-center align-items-center">
                          <span className={styles.LabelText}>Дата рождения | Основание | Начало</span>
                          <InputMask
                            type={"text"}
                            mask="99.99.9999"
                            className={clsInputDate1.join(" ")}
                            name="date"
                            placeholder="ДД.ММ.ГГГГ"
                            value={date1}
                            onChange={(e) => this.props.onChangeDate1(e)}
                          />
                          <span className={styles.LabelText}>День Месяц Год</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-12 col-md-6">
                  <div className="row">
                    <div className="col-12">
                      <div className="d-flex justify-content-center align-items-center">
                        <label className="d-flex flex-column justify-content-center align-items-center">
                          <span className={styles.LabelText}>Имя | Наименование | Слово</span>
                          <input type="text" className={clsInputName2.join(" ")} value={name2} onChange={(e) => this.props.onChangeName2(e)} />
                          <span className={`d-flex flex-column justify-content-center align-items-center mb-2 ${styles.LabelText}`}>
                            <span>Ввод возможен на русском</span>
                            <span>латинице | +цифры</span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  {isSuccess ? (
                    <div className="row">
                      <div className="col-12">
                        <div className="d-flex justify-content-center align-items-center">
                          {values.Values2.ValuesNumberPos.map((valueNumberPos, index) => {
                            return <AlphabetChar key={index} ValueNumberPosValue={valueNumberPos[0]} ValueNumberPosNumber={valueNumberPos[1]} />;
                          })}
                        </div>
                      </div>
                      <div className="col-12">
                        <div className={`my-3 ${styles.ItemResContainer}`}>
                          <div className={styles.ItemRes}>
                            <div className={styles.ItemResTitle}>Цель</div>
                            <div className={styles.ItemResValue}>{values.Values2.ValueTarget}</div>
                          </div>
                          <div className={styles.ItemRes}>
                            <div className={styles.ItemResTitle}>Сила</div>
                            <div className={styles.ItemResValue}>{values.Values2.ValuePower}</div>
                          </div>
                          <div className={styles.ItemRes}>
                            <div className={styles.ItemResTitle}>Тень</div>
                            <div className={styles.ItemResValue}>{values.Values2.ValueShadow}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : null}
                  <div className="row">
                    <div className="col-12">
                      <div className="d-flex justify-content-center align-items-center">
                        <label className="d-flex flex-column justify-content-center align-items-center">
                          <span className={styles.LabelText}>Дата рождения | Основание | Начало</span>
                          <InputMask
                            type={"text"}
                            mask="99.99.9999"
                            className={clsInputDate2.join(" ")}
                            name="date"
                            placeholder="ДД.ММ.ГГГГ"
                            value={date2}
                            onChange={(e) => this.props.onChangeDate2(e)}
                          />
                          <span className={styles.LabelText}>День Месяц Год</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                {isSuccess ? (
                  <div className="col-12 d-flex justify-content-center align-items-center">
                    <button className="btn btn-primary btn-lg my-3 px-5 text-light btn-w-250" onClick={(e) => this.props.onChangeAction("CompositePortrait")}>
                      Открыть
                    </button>
                  </div>
                ) : null}
                <div className="col-12">
                  <div className="row pt-3">
                    <div className="col-12 col-md-4 d-flex justify-content-center justify-content-md-end align-items-center">
                      <Link to="/calc-numerologist-luna-shafran/composite-portrait">
                        <button className="btn btn-link text-decoration-none ">
                          <span className="me-1">Композитный портрет</span>
                          <img src={imgArrowLeftBlack} alt="<" className={styles.ImgIcon} />
                        </button>
                      </Link>
                    </div>
                    <div className="col-12 col-md-4 d-flex justify-content-center align-items-center">
                      <button className="btn btn-primary btn-lg my-3 px-5 text-light btn-w-250" onClick={this.props.handleCalc}>
                        Рассчитать
                      </button>
                    </div>
                    <div className="col-12 col-md-4 d-flex justify-content-center justify-content-md-start align-items-center">
                      <Link to="/calc-numerologist-luna-shafran/">
                        <button className="btn btn-link text-decoration-none">
                          <img src={imgArrowRightBlack} alt=">" className={styles.ImgIcon} />
                          <span className="ms-1">Ввод даты</span>
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </CardPortrait>
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default CommmonPortraitForm;
