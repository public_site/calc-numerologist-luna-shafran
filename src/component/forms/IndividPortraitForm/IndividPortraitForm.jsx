import React, { Component } from "react";
import styles from "./IndividPortraitForm.module.scss";
import InputMask from "react-input-mask";
import { CardPortrait } from "../../UI/CardPortrait/CardPortrait";
class IndividPortraitForm extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      IsModalShow: true,
      IsModalShow1: false,
    };
  }

  handleShowModal = (isModalShow) => {
    this.setState({
      IsModalShow: isModalShow,
    });
  };
  handleShowModal1 = (isModalShow) => {
    this.setState({
      IsModalShow1: isModalShow,
    });
  };
  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var preAction = this.props.PreAction;
    var dataForm = this.props.DataForm;

    var titleBasePortrait = "Показывает видимую основу личности и заложенный датой рождения потенциал человека, который формируется и трансформируется на протяжении всего жизненного цикла.";

    var clsDisabled = "";
    if (!this.props.IsSuccess) {
      clsDisabled = styles.Disabled;
    }

    var clsRowButtonSubmit = ["row"];
    var clsInputDate = ["form-control", styles.InputForm];
    var clsInputName = ["form-control", styles.InputForm];
    if (!dataForm.InputDate.Validate || !dataForm.InputName.Validate) {
      if (!dataForm.InputDate.Validate) {
        clsInputDate.push("is-invalid");
      }
      if (!dataForm.InputName.Validate) {
        clsInputName.push("is-invalid");
      }
    }
    if (dataForm.InputDate.Value == "" || dataForm.InputName.Value == "") {
      clsRowButtonSubmit.push(styles.Disabled);
    }
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12 text-center">
            <h1 className="h3 text-uppercase my-3">Психологический Портрет</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <CardPortrait IsBackgroundColor={true}>
              <div className="row">
                <div className="col-12 col-sm-6">
                  <div className="d-flex justify-content-center align-items-center">
                    <label className="d-flex flex-column justify-content-center align-items-center">
                      <span className={styles.LabelText}>введите дату рождения</span>
                      <InputMask
                        type={"text"}
                        mask="99.99.9999"
                        className={clsInputDate.join(" ")}
                        name="date"
                        placeholder="ДД.ММ.ГГГГ"
                        value={dataForm.InputDate.Value}
                        onChange={this.props.onChangeDate}
                      />
                      <span className={styles.LabelText}>День Месяц Год</span>
                    </label>
                  </div>
                </div>
                <div className="col-12 col-sm-6">
                  <div className="d-flex justify-content-center align-items-center">
                    <label className="d-flex flex-column justify-content-center align-items-center">
                      <span className={styles.LabelText}>введите имя</span>
                      <input type="text" className={clsInputName.join(" ")} value={dataForm.InputName.Value} onChange={this.props.onChangeName} />
                      <span className={`d-flex justify-content-center align-items-end ${styles.LabelText}`}>
                        <span>Архетип имени: </span> <span className={`text-primary ms-1 ${styles.TextNameArchetype}`}>{this.props.NameArchetype}</span>
                      </span>
                    </label>
                  </div>
                </div>
              </div>
              <div className={clsRowButtonSubmit.join(" ")}>
                <div className="col-12 d-flex justify-content-center align-items-center">
                  <button className="btn btn-primary btn-lg my-3 px-5 text-light" onClick={this.props.handleSubmitCalculation}>
                    Рассчитать
                  </button>
                </div>
              </div>
              <div className={`row ${clsDisabled}`}>
                <div className="col-12">
                  <div className="d-flex flex-column flex-md-row justify-content-center align-items-center">
                    <button className="btn btn-link" onClick={(e) => this.props.onChangePreAction("FullIndividPortrait")}>
                      {preAction == "FullIndividPortrait" ? (
                        <u>
                          <b>Индивидуальный Портрет (PRO)</b>
                        </u>
                      ) : (
                        "Индивидуальный Портрет (PRO)"
                      )}
                      ;
                    </button>
                    <button className="btn btn-link" title={titleBasePortrait} onClick={(e) => this.props.onChangePreAction("BasePortrait")}>
                      {preAction == "BasePortrait" ? (
                        <u>
                          <b>Базовый Портрет | Периоды Жизни</b>
                        </u>
                      ) : (
                        "Базовый Портрет | Периоды Жизни"
                      )}
                    </button>
                  </div>
                </div>
                <div className="col-12 d-flex flex-column flex-md-row justify-content-center align-items-center">
                  <div className="d-flex flex-column flex-md-row">
                    <div className="form-check form-switch mx-2">
                      <label className="d-flex align-items-center">
                        <input className={`form-check-input me-2`} type="checkbox" role="switch" checked={this.props.IsVisualTaro} onChange={this.props.onChangeCheckboxCardTaro} />
                        <span>Визуал в Таро</span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </CardPortrait>
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default IndividPortraitForm;
