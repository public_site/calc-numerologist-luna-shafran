import React, { Component } from "react";
import Container from "../../hoc/Container/Container";
import styles from "./Header.module.scss";
import imgLogo from "./../../img/logo.png";
import { IoMenuOutline, IoCloseOutline } from "react-icons/io5";
import { BsTelegram, BsWhatsapp } from "react-icons/bs";
import { connect } from "react-redux";
import { PortraitAction } from "../../store/actions/PortraitAction";
import { Link } from "react-router-dom";

class Header extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      IsMenu: false,
    };
  }

  handleRefresh = (e) => {
    var isMenu = !this.state.IsMenu;
    this.setState({
      IsMenu: isMenu,
    });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var clsMenu = [styles.Menu];
    if (this.state.IsMenu) {
      clsMenu.push(styles.MenuActive);
    }

    var clsWrapper = [styles.Wrapper];
    if (this.state.IsMenu) {
      clsWrapper.push(styles.WrapperActive);
    }

    return (
      <div className={styles.Header}>
        <div className={clsWrapper.join(" ")} onClick={(e) => this.handleRefresh(e)}></div>
        <div className={`container ${styles.HeaderContainer}`}>
          <div className="row bg-white">
            <div className="col-12">
              <div className="d-flex flex-column justify-content-center align-items-center">
                <img src={imgLogo} alt="LunaChafran" className={styles.ImgLogo} />
              </div>
              <div className="d-flex justify-content-center align-items-center">
                <div className="d-flex justify-content-end align-items-center">
                  <a
                    href="https://zen.yandex.ru/media/id/62398e1a8e0938497dd8fe2f/zapusk-svoego-onlainkalkuliatora-zaplanirovan-na-pervuiu-polovinu-maia-dlia-628917b7bd4b132054ca52a7"
                    target="_blank"
                    className="btn btn-link m-0 p-0 text-light d-flex align-items-center"
                  >
                    <span>@Archetypes5D</span>
                  </a>
                </div>
                <div className="d-flex justify-content-center align-items-center">
                  <span className="mx-1">|</span>
                </div>
                <div className="d-flex justify-content-start align-items-center">
                  <a href="https://vk.com/" target="_blank" className="btn btn-link m-0 p-0 text-light d-flex align-items-center">
                    <span>@LunaChafran</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <button className={`btn ${styles.ButtonMenu}`} onClick={(e) => this.handleRefresh(e)}>
            {this.state.IsMenu ? <IoCloseOutline /> : <IoMenuOutline />}
          </button>
        </div>
        <div className={clsMenu.join(" ")}>
          <div className="container position-relative d-flex flex-column">
            <div className={styles.MenuList}>
              <Link to="/calc-numerologist-luna-shafran/">
                <button className="btn btn-link">Психологический Портрет</button>
              </Link>
              <Link to="/calc-numerologist-luna-shafran/alphabet-portrait-1">
                <button className="btn btn-link">Алфавитный портрет (нумерологический)</button>
              </Link>
              <Link to="/calc-numerologist-luna-shafran/alphabet-portrait-2">
                <button className="btn btn-link">Алфавитный портрет (А. Хшановской)</button>
              </Link>
              <Link to="/calc-numerologist-luna-shafran/composite-portrait">
                <button className="btn btn-link">Композитный портрет</button>
              </Link>
              <Link to="/calc-numerologist-luna-shafran/common-portrait">
                <button className="btn btn-link">Портрет Совместимости</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {}
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  let namePortrait = state.portrait.NamePortrait;
  return {
    NamePortrait: namePortrait,
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    setPortraitAction: (namePortrait) => dispatch(PortraitAction(namePortrait)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
