import React, { Component } from "react";
import CompositePortraitForm from "../../component/forms/CompositePortraitForm/CompositePortraitForm";
import styles from "./CompositePortraitPage.module.scss";
import AlphabetPortraitAnalysis from "../../component/AlphabetPortraitAnalysis/AlphabetPortraitAnalysis";
import { validDate, getPeriod, calcAlphabet, calcIndividPortrait, calcTwoPortrait, convertToRoman } from "../../Func";
import CompositePortraitPrint from "../../component/CompositePortraitPrint/CompositePortraitPrint";

class CompositePortraitPage extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      Action: "Form",
      PreAction: "",
      NameArchetype: "",
      IsSuccess: false,
      DataForm: {
        InputDate1: {
          Value: "", // 23.05.1977
          Validate: true,
        },
        InputName1: {
          Value: "", // Луна
          Validate: true,
        },
        InputDate2: {
          Value: "", // 23.05.1977
          Validate: true,
        },
        InputName2: {
          Value: "", // Луна
          Validate: true,
        },
      },
      Values: {
        Values1: {},
        Values2: {},
        Values3: {},
      },
    };
  }

  /**
   * Ввод даты 1
   * @param {*} e
   */
  onChangeDate1 = (e) => {
    var dataForm = this.state.DataForm;
    dataForm.InputDate1.Value = e.target.value;
    dataForm.InputDate1.Validate = validDate(e.target.value) ? true : false;
    this.setState({
      DataForm: dataForm,
    });
  };
  /**
   * Ввод имени 1
   * @param {*} e
   */
  onChangeName1 = (e) => {
    var dataForm = this.state.DataForm;
    dataForm.InputName1.Value = e.target.value;
    dataForm.InputName1.Validate = e.target.value === "" ? false : true;
    this.setState({
      DataForm: dataForm,
    });
  };
  /**
   * Ввод даты 2
   * @param {*} e
   */
  onChangeDate2 = (e) => {
    var dataForm = this.state.DataForm;
    dataForm.InputDate2.Value = e.target.value;
    dataForm.InputDate2.Validate = validDate(e.target.value) ? true : false;
    this.setState({
      DataForm: dataForm,
    });
  };
  /**
   * Ввод имени 2
   * @param {*} e
   */
  onChangeName2 = (e) => {
    var dataForm = this.state.DataForm;
    dataForm.InputName2.Value = e.target.value;
    dataForm.InputName2.Validate = e.target.value === "" ? false : true;
    this.setState({
      DataForm: dataForm,
    });
  };

  /**
   * Смена формы
   * @param {*} nameAction
   */
  onChangeAction = (nameAction) => {
    this.setState({
      Action: nameAction,
    });
  };

  handleCalc = (e) => {
    var dataForm = this.state.DataForm;
    var name1 = this.state.DataForm.InputName1.Value;
    var date1 = this.state.DataForm.InputDate1.Value;
    var name2 = this.state.DataForm.InputName2.Value;
    var date2 = this.state.DataForm.InputDate2.Value;
    var isSuccess = true;

    dataForm.InputName1.Validate = name1 === "" ? false : true;
    dataForm.InputDate1.Validate = validDate(date1) ? true : false;
    dataForm.InputName2.Validate = name2 === "" ? false : true;
    dataForm.InputDate2.Validate = validDate(date2) ? true : false;

    if (!dataForm.InputName1.Validate || !dataForm.InputDate1.Validate || !dataForm.InputName2.Validate || !dataForm.InputDate2.Validate) {
      isSuccess = false;
    }
    if (!isSuccess) {
      this.setState({
        IsSuccess: isSuccess,
        DataForm: dataForm,
      });
      return;
    }

    var values = {
      Values1: {},
      Values2: {},
      Values3: {},
    };

    values.Values1 = calcAlphabet(name1, values.Values1);
    values.Values1 = calcIndividPortrait(this.state.DataForm.InputDate1.Value + "", values.Values1);
    values.Values2 = calcAlphabet(name2, values.Values2);
    values.Values2 = calcIndividPortrait(this.state.DataForm.InputDate2.Value + "", values.Values2);
    values.Values3 = calcTwoPortrait(values.Values1, values.Values2);

    values.Period1 = getPeriod(date1);
    values.Period2 = getPeriod(date2);

    var numberSelected = {};

    for (var key1 in values.Values1) {
      if (values.Values1[key1] == "м") continue;
      if (values.Values1[key1] == "ж") continue;
      if (values.Values1[key1] == "н") continue;
      if (key1.indexOf("ValuePosition") == -1) continue;
      for (var key2 in values.Values2) {
        if (key1 == key2) {
          if (values.Values1[key1] == values.Values2[key2]) {
            numberSelected[key1] = values.Values1[key1];
          }
        }
      }
    }

    for (var key1 in values.Values1) {
      if (values.Values1[key1] == "м") continue;
      if (values.Values1[key1] == "ж") continue;
      if (values.Values1[key1] == "н") continue;
      if (key1.indexOf("ValuePosition") == -1) continue;
      for (var key2 in values.Values3) {
        if (key1 == key2) {
          if (values.Values1[key1] == values.Values3[key2]) {
            numberSelected[key1] = values.Values1[key1];
          }
        }
      }
    }

    for (var key1 in values.Values2) {
      if (values.Values2[key1] == "м") continue;
      if (values.Values2[key1] == "ж") continue;
      if (values.Values2[key1] == "н") continue;
      if (key1.indexOf("ValuePosition") == -1) continue;
      for (var key2 in values.Values3) {
        if (key1 == key2) {
          if (values.Values2[key1] == values.Values3[key2]) {
            numberSelected[key1] = values.Values2[key1];
          }
        }
      }
    }

    console.log(numberSelected);
    this.setState({
      IsSuccess: isSuccess,
      Values: values,
      NumberSelected: numberSelected,
    });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var action = "form";
    if (this.state.Action === "Form") {
      action = this.state.Action;
    }

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12 text-center">
            <h1 className="h3 text-uppercase my-3">Композитный портрет</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            {action === "Form" ? (
              <CompositePortraitForm
                DataForm={this.state.DataForm}
                Values={this.state.Values}
                onChangeName1={this.onChangeName1}
                onChangeDate1={this.onChangeDate1}
                onChangeName2={this.onChangeName2}
                onChangeDate2={this.onChangeDate2}
                handleCalc={this.handleCalc}
                IsSuccess={this.state.IsSuccess}
                onChangeAction={this.onChangeAction}
              />
            ) : null}
            {this.state.Action == "CompositePortrait" ? (
              <CompositePortraitPrint NumberSelected={this.state.NumberSelected} DataForm={this.state.DataForm} Values={this.state.Values} onChangeAction={this.onChangeAction} />
            ) : null}
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default CompositePortraitPage;
