import React, { Component } from "react";
import { connect } from "react-redux";
import AlphabetPortraitForm2 from "../../component/forms/AlphabetPortraitForm2/AlphabetPortraitForm2";
import AlphabetPortraitAnalysis2 from "../../component/AlphabetPortraitAnalysis2/AlphabetPortraitAnalysis2";

import { validDate, calcAlphabet, calcAlphabet2, calcIndividPortrait, getMainArcaneForKhrzanowska } from "../../Func";

class AlphabetPortraitPage2 extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      Action: "AlphabetPortrait",
      PreAction: "",
      NameArchetype: "",
      IsSuccess: false,
      DataForm: {
        InputDate: {
          Value: "", // 23.05.1977
          Validate: true,
        },
        InputName: {
          Value: "", // Луна
          Validate: true,
        },
      },
      Values: {
        ValueTarget: [],
        ValuePower: [],
        ValueShadow: [],
        ValuesNumberPos: [],
      },
      NameValues: [],
    };
  }

  /**
   * Ввод даты
   * @param {*} e
   */
  onChangeDate = (e) => {
    var dataForm = this.state.DataForm;
    dataForm.InputDate.Value = e.target.value;
    dataForm.InputDate.Validate = validDate(e.target.value) ? true : false;
    this.setState({
      DataForm: dataForm,
    });
  };
  /**
   * Ввод имени
   * @param {*} e
   */
  onChangeName = (e) => {
    var dataForm = this.state.DataForm;
    dataForm.InputName.Value = e.target.value;
    dataForm.InputName.Validate = e.target.value === "" ? false : true;
    this.setState({
      DataForm: dataForm,
    });
  };

  onChangeAction = (nameAction) => {
    this.setState({
      Action: nameAction,
    });
  };

  /**
   * Рассчитать
   * @param {*} e
   */
  handleCalc = (e) => {
    var dataForm = this.state.DataForm;
    var name = this.state.DataForm.InputName.Value;
    var date = this.state.DataForm.InputDate.Value;

    var isSuccess = true;
    dataForm.InputName.Validate = name == "" ? false : true;
    dataForm.InputDate.Validate = validDate(date) ? true : false;

    if (!dataForm.InputName.Validate || !dataForm.InputDate.Validate) {
      isSuccess = false;
    }
    if (!isSuccess) {
      this.setState({
        IsSuccess: isSuccess,
        DataForm: dataForm,
      });
      return;
    }

    var values = this.state.Values;
    var nameValues = [];
    name = name.replace(/\s/g, "");
    name.split(" ").forEach(function (n) {
      var itemValue = {
        ValueName: n,
        ValueTarget: "",
        ValuePower: "",
        ValueShadow: "",
        MainArcane: "",
        ValuesNumberPos: [],
      };
      itemValue = calcAlphabet2(n, itemValue);
      itemValue.MainArcane = getMainArcaneForKhrzanowska(n);
      nameValues.push(itemValue);

      itemValue.ValueTarget.forEach((value) => {
        values.ValueTarget.push(value);
      });
      itemValue.ValuePower.forEach((value) => {
        values.ValuePower.push(value);
      });
      itemValue.ValueShadow.forEach((value) => {
        values.ValueShadow.push(value);
      });
    });
    values = calcAlphabet2(name, values);
    values = calcIndividPortrait(date + "", values);

    this.setState({
      IsSuccess: isSuccess,
      NameValues: nameValues,
      Values: values,
    });
  };
  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return (
      <React.Fragment>
        {this.state.Action == "AlphabetPortrait" ? (
          <AlphabetPortraitForm2
            DataForm={this.state.DataForm}
            Values={this.state.Values}
            NameValues={this.state.NameValues}
            onChangeName={this.onChangeName}
            onChangeDate={this.onChangeDate}
            handleCalc={this.handleCalc}
            IsSuccess={this.state.IsSuccess}
            onChangeAction={this.onChangeAction}
          />
        ) : null}
        {this.state.Action == "AlphabetPortraitAnalysis" ? (
          <AlphabetPortraitAnalysis2 DataForm={this.state.DataForm} Values={this.state.Values} NameValues={this.state.NameValues} onChangeAction={this.onChangeAction} IsPrint={false} />
        ) : null}
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    //
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    //
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AlphabetPortraitPage2);
