import { PORTRAIT_SET } from "../actions/ActionTypes";

const initialState = {
  namePortrait: "",
};

export default function PortraitReducer(state = initialState, action) {
  switch (action.type) {
    case PORTRAIT_SET:
      return {
        ...state,
        namePortrait: action.namePortrait,
      };
    default:
      return state;
  }
}
