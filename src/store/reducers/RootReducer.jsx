import { combineReducers } from "redux";
import PortraitReducer from "./PortraitReducer";

export default combineReducers({
  portrait: PortraitReducer,
});
