import { PORTRAIT_SET } from "./ActionTypes";

export function PortraitAction(namePortrait) {
  return async (dispatch) => {
    dispatch({ type: PORTRAIT_SET, namePortrait });
  };
}
