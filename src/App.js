import './App.scss';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Container from './hoc/Container/Container';
import Header from './component/Header/Header';
import HomePage from './pages/HomePage/HomePage';
import AlphabetPortraitPage from './pages/AlphabetPortraitPage/AlphabetPortraitPage';
import AlphabetPortraitPage2 from './pages/AlphabetPortraitPage2/AlphabetPortraitPage2';
import CompositePortraitPage from './pages/CompositePortraitPage/CompositePortraitPage';
import CommmonPortraitPage from './pages/CommmonPortraitPage/CommmonPortraitPage';

function App() {

  return (
    <div className='app'>
      <BrowserRouter>
        <Header />
        <Container>
          <Routes>
            <Route path="*" element={<HomePage />} exact />
            <Route path="calc-numerologist-luna-shafran/" element={<HomePage />} exact />
            <Route path="calc-numerologist-luna-shafran/alphabet-portrait-1" element={<AlphabetPortraitPage />} exact />
            <Route path="calc-numerologist-luna-shafran/alphabet-portrait-2" element={<AlphabetPortraitPage2 />} exact />
            <Route path="calc-numerologist-luna-shafran/composite-portrait" element={<CompositePortraitPage />} exact />
            <Route path="calc-numerologist-luna-shafran/common-portrait" element={<CommmonPortraitPage />} exact />
          </Routes>
        </Container>
      </BrowserRouter>
    </div>
  );
}


export default App
