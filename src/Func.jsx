/**
 * Проверка даты
 * @param {*} dtString
 * @returns
 */
export function validDate(dtString) {
  dtString = dtString.split("_").join("");
  var re = /^\d{1,2}\.\d{1,2}\.\d{4}$/;
  if (dtString !== "" && !dtString.match(re)) {
    return false;
  }
  var aDate = dtString.split(".");
  if (aDate.length !== 3) {
    return false;
  }
  var nDay = parseInt(aDate[0]);
  if (nDay > 31) {
    return false;
  }
  var nMonth = parseInt(aDate[1]);
  if (nMonth > 12) {
    return false;
  }
  return true;
}

export function convert22(value) {
  value = parseInt(value);
  if (value === 0) {
    return 22;
  }
  while (value > 22) {
    value = value - 22;
  }
  return value;
}
export function convert10(value) {
  value = parseInt(value);
  var valueNew = 0;
  if (value > 9) {
    valueNew = Math.floor(value / 10) + (value % 10);
  } else {
    valueNew = value;
  }
  return valueNew;
}

/**
 * Конвертировать в римские цифры
 * @param {*} num
 * @returns
 */
export function convertToRoman(num) {
  var roman = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1,
  };
  var str = "";

  for (var i of Object.keys(roman)) {
    var q = Math.floor(num / roman[i]);
    num -= q * roman[i];
    str += i.repeat(q);
  }

  return str;
}

export function convertToArab(text) {
  var font_ar = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000, 4000, 5000, 9000, 10000];
  var font_rom = ["I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M", "M&#8577;", "&#8577;", "&#8577;&#8578;", "&#8578;"];

  var text = text.toUpperCase();
  var rezult = 0;
  var posit = 0;
  var n = font_ar.length - 1;
  while (n >= 0 && posit < text.length) {
    if (text.substr(posit, font_rom[n].length) == font_rom[n]) {
      rezult += font_ar[n];
      posit += font_rom[n].length;
    } else n--;
  }
  return rezult;
}

export function getArchetype(number) {
  number = parseInt(number);
  switch (number) {
    case 1:
      return "м";
    case 2:
      return "ж";
    case 3:
      return "ж";
    case 4:
      return "м";
    case 5:
      return "м";
    case 6:
      return "н";
    case 7:
      return "м";
    case 8:
      return "ж";
    case 9:
      return "н";
    case 10:
      return "н";
    case 11:
      return "м";
    case 12:
      return "н";
    case 13:
      return "ж";
    case 14:
      return "н";
    case 15:
      return "н";
    case 16:
      return "н";
    case 17:
      return "ж";
    case 18:
      return "ж";
    case 19:
      return "м";
    case 20:
      return "н";
    case 21:
      return "н";
    case 22:
      return "н";
    default:
      return "н";
  }
}

export function getNameArchetype(name, isConvertToRoman) {
  name = (name + "").toUpperCase();
  var number = 0;
  for (let i = 0; i < name.length; i++) {
    switch (name[i]) {
      case "А":
      case "И":
      case "С":
      case "Ъ":
      case "A":
      case "J":
      case "S":
        number += 1;
        break;
      case "Б":
      case "Й":
      case "Т":
      case "Ы":
      case "B":
      case "K":
      case "T":
        number += 2;
        break;
      case "В":
      case "К":
      case "У":
      case "Ь":
      case "C":
      case "L":
      case "U":
        number += 3;
        break;
      case "Г":
      case "Л":
      case "Ф":
      case "Э":
      case "D":
      case "M":
      case "V":
        number += 4;
        break;
      case "Д":
      case "М":
      case "Х":
      case "Ю":
      case "E":
      case "N":
      case "W":
        number += 5;
        break;
      case "Е":
      case "Н":
      case "Ц":
      case "Я":
      case "F":
      case "O":
      case "X":
        number += 6;
        break;
      case "Ё":
      case "О":
      case "Ч":
      case "G":
      case "P":
      case "Y":
        number += 7;
        break;
      case "Ж":
      case "П":
      case "Ш":
      case "H":
      case "Q":
      case "Z":
        number += 8;
        break;
      case "З":
      case "Р":
      case "Щ":
      case "I":
      case "R":
        number += 9;
        break;
      default:
        break;
    }
  }

  number = convert22(number);
  if (!!isConvertToRoman) {
    return convertToRoman(number);
  } else {
    return number;
  }
}

export function getPositionCharInAlphabet(char) {
  var alphabetRus = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя".toUpperCase();
  var alphabetEng = "abcdefghijklmnopqrstuvwxyz".toUpperCase();
  char = (char + "").toUpperCase();
  var pos = alphabetRus.indexOf(char);
  if (pos == -1) {
    pos = alphabetEng.indexOf(char);
  }
  if (pos == -1) {
    pos = char;
  } else {
    pos++;
  }
  return pos;
}

export function calcAlphabet(name, values) {
  var countCharAll = 0;
  var countCharGlasnyye = 0;
  var countCharSoglasnyye = 0;
  name
    .toUpperCase()
    .split("")
    .forEach((char) => {
      countCharAll += getNameArchetype(char);
      switch (char) {
        case "А":
        case "Ё":
        case "Е":
        case "И":
        case "О":
        case "У":
        case "Ы":
        case "Э":
        case "Я":
        case "Ю":
        case "A":
        case "E":
        case "I":
        case "O":
        case "U":
        case "Y":
          countCharGlasnyye += getNameArchetype(char);
          break;
        default:
          countCharSoglasnyye += getNameArchetype(char);
          break;
      }
    });

  var valueTarget = convert22(parseInt(countCharAll));
  var valuePower = convert22(parseInt(countCharSoglasnyye));
  var valueShadow = convert22(parseInt(countCharGlasnyye));
  var valuesNumberPos = [];
  name
    .toUpperCase()
    .split("")
    .forEach((ch) => {
      var pos = getPositionCharInAlphabet(ch);
      pos = convert10(pos);
      var valueNumberPos = [ch, pos];
      valuesNumberPos.push(valueNumberPos);
    });

  values.ValueTarget = [convertToRoman(valueTarget)];
  values.ValuePower = [convertToRoman(valuePower)];
  values.ValueShadow = [convertToRoman(valueShadow)];
  values.ValuesNumberPos = valuesNumberPos;

  return values;
}

export function calcAlphabet2(name, values) {
  var countCharAll = 0;
  var countCharGlasnyye = 0;
  var countCharSoglasnyye = 0;
  name
    .toUpperCase()
    .split("")
    .forEach((char) => {
      countCharAll += getNameArchetype(char);
      switch (char) {
        case "А":
        case "Ё":
        case "Е":
        case "И":
        case "О":
        case "У":
        case "Ы":
        case "Э":
        case "Я":
        case "Ю":
        case "A":
        case "E":
        case "I":
        case "O":
        case "U":
        case "Y":
          countCharGlasnyye += getNameArchetype(char);
          break;
        default:
          countCharSoglasnyye += getNameArchetype(char);
          break;
      }
    });

  var valueTarget = convert22(parseInt(countCharAll));
  var valuePower = convert22(parseInt(countCharSoglasnyye));
  var valueShadow = convert22(parseInt(countCharGlasnyye));
  var valuesNumberPos = [];
  name
    .toUpperCase()
    .split("")
    .forEach((ch) => {
      var pos = getPositionForKhrzanowska(ch);
      var valueNumberPos = [ch, pos];
      valuesNumberPos.push(valueNumberPos);
    });

  values.ValueTarget = [convertToRoman(valueTarget)];
  values.ValuePower = [convertToRoman(valuePower)];
  values.ValueShadow = [convertToRoman(valueShadow)];
  values.ValuesNumberPos = valuesNumberPos;

  return values;
}

export function calcIndividPortrait(dtString, values) {
  var aDate = dtString.split(".");
  var nDay = parseInt(aDate[0]);
  var nMonth = parseInt(aDate[1]);
  var nYear = parseInt(aDate[2]);

  // Перевод даты рождения в Архетипы = Арканы Таро по системе таронумерологии
  var ValuePosition1 = nDay;
  if (ValuePosition1 > 22) {
    ValuePosition1 = ValuePosition1 - 22;
  }

  var ValuePosition2 = nMonth;
  var ValuePosition3 = 0;
  var arr = (nYear + "").split("");
  for (let i = 0; i < arr.length; i++) {
    ValuePosition3 += parseInt(arr[i]);
  }

  ValuePosition3 = convert22(parseInt(ValuePosition3));

  // Расчет периоды жизни

  var valuePosition4 = convert22(ValuePosition1 + ValuePosition2);
  var valuePosition5 = convert22(ValuePosition2 + ValuePosition3);
  var valuePosition6 = convert22(valuePosition4 + valuePosition5);
  var valuePosition7 = convert22(ValuePosition1 + valuePosition5);
  var valuePosition8 = convert22(ValuePosition2 + valuePosition6);
  var valuePosition12 = convert22(valuePosition4 + valuePosition5 + valuePosition6);
  var valuePosition13 = convert22(ValuePosition1 + valuePosition4 + valuePosition6);
  var valuePosition14 = convert22(ValuePosition3 + valuePosition5 + valuePosition6);

  var valuePosition9 = convert22(Math.abs(ValuePosition1 - ValuePosition2));
  var valuePosition10 = convert22(Math.abs(ValuePosition2 - ValuePosition3));
  var valuePosition11 = convert22(Math.abs(valuePosition9 - valuePosition10));
  var valuePosition15 = convert22(Math.abs(valuePosition9 + valuePosition10 + valuePosition11 - valuePosition7));
  var valuePosition16 = convert22(ValuePosition1 + valuePosition4 + valuePosition5 + ValuePosition3);
  var valuePosition17 = convert22(valuePosition11 + valuePosition6);
  var valuePosition18 = convert22(valuePosition11 + valuePosition8);

  var valuePosition30 = convert22(valuePosition12 * 2);

  var valuePositionA = convert22(ValuePosition1 + valuePosition4);
  var valuePositionB = convert22(ValuePosition2 + valuePosition4);
  var valuePositionC = convert22(ValuePosition2 + valuePosition5);
  var valuePositionD = convert22(ValuePosition3 + valuePosition5);
  var valuePositionE = convert22(valuePosition4 + valuePosition6);
  var valuePositionF = convert22(valuePosition5 + valuePosition6);
  var valuePositionH = convert22(valuePositionA + valuePositionE);

  values.ValuePosition1 = convertToRoman(ValuePosition1);
  values.ValueSupPosition1 = getArchetype(ValuePosition1);
  values.ValuePosition2 = convertToRoman(ValuePosition2);
  values.ValueSupPosition2 = getArchetype(ValuePosition2);
  values.ValuePosition3 = convertToRoman(ValuePosition3);
  values.ValueSupPosition3 = getArchetype(ValuePosition3);
  values.ValuePosition4 = convertToRoman(valuePosition4);
  values.ValueSupPosition4 = getArchetype(valuePosition4);
  values.ValuePosition5 = convertToRoman(valuePosition5);
  values.ValueSupPosition5 = getArchetype(valuePosition5);
  values.ValuePosition6 = convertToRoman(valuePosition6);
  values.ValueSupPosition6 = getArchetype(valuePosition6);
  values.ValuePosition7 = convertToRoman(valuePosition7);
  values.ValueSupPosition7 = getArchetype(valuePosition7);
  values.ValuePosition8 = convertToRoman(valuePosition8);
  values.ValueSupPosition8 = getArchetype(valuePosition8);
  values.ValuePosition9 = convertToRoman(valuePosition9);
  values.ValuePosition10 = convertToRoman(valuePosition10);
  values.ValuePosition11 = convertToRoman(valuePosition11);
  values.ValuePosition12 = convertToRoman(valuePosition12);
  values.ValuePosition13 = convertToRoman(valuePosition13);
  values.ValuePosition14 = convertToRoman(valuePosition14);
  values.ValuePosition15 = convertToRoman(valuePosition15);
  values.ValuePosition16 = convertToRoman(valuePosition16);
  values.ValuePosition17 = convertToRoman(valuePosition17);
  values.ValuePosition18 = convertToRoman(valuePosition18);
  values.ValuePositionA = convertToRoman(valuePositionA);
  values.ValuePositionB = convertToRoman(valuePositionB);
  values.ValuePositionC = convertToRoman(valuePositionC);
  values.ValuePositionD = convertToRoman(valuePositionD);
  values.ValuePositionE = convertToRoman(valuePositionE);
  values.ValuePositionF = convertToRoman(valuePositionF);
  values.ValuePositionH = convertToRoman(valuePositionH);
  values.ValuePosition30 = convertToRoman(valuePosition30);

  return values;
}

export function calcTwoPortrait(individPortraitValues1, individPortraitValues2) {
  var values = {};
  values.ValuePosition1 = convert22(convertToArab(individPortraitValues1.ValuePosition1) + convertToArab(individPortraitValues2.ValuePosition1));
  values.ValuePosition2 = convert22(convertToArab(individPortraitValues1.ValuePosition2) + convertToArab(individPortraitValues2.ValuePosition2));
  values.ValuePosition3 = convert22(convertToArab(individPortraitValues1.ValuePosition3) + convertToArab(individPortraitValues2.ValuePosition3));
  values.ValuePosition4 = convert22(values.ValuePosition1 + values.ValuePosition2);
  values.ValuePosition5 = convert22(values.ValuePosition2 + values.ValuePosition3);
  values.ValuePosition6 = convert22(values.ValuePosition4 + values.ValuePosition5);
  values.ValuePosition7 = convert22(values.ValuePosition1 + values.ValuePosition2 + values.ValuePosition3);
  values.ValuePosition8 = convert22(values.ValuePosition2 + values.ValuePosition6);
  values.ValuePosition12 = convert22(values.ValuePosition4 + values.ValuePosition5 + values.ValuePosition6);
  values.ValuePosition13 = convert22(values.ValuePosition1 + values.ValuePosition4 + values.ValuePosition6);
  values.ValuePosition14 = convert22(values.ValuePosition3 + values.ValuePosition5 + values.ValuePosition6);
  values.ValuePosition16 = convert22(values.ValuePosition1 + values.ValuePosition4 + values.ValuePosition5 + values.ValuePosition3);
  values.ValuePosition9 = convert22(Math.abs(values.ValuePosition1 - values.ValuePosition2));
  values.ValuePosition10 = convert22(Math.abs(values.ValuePosition2 - values.ValuePosition3));
  values.ValuePosition11 = convert22(Math.abs(values.ValuePosition9 - values.ValuePosition10));
  values.ValuePosition15 = convert22(Math.abs(values.ValuePosition9 + values.ValuePosition10 + values.ValuePosition11 - values.ValuePosition7));
  values.ValuePosition17 = convert22(values.ValuePosition11 + values.ValuePosition6);
  values.ValuePosition18 = convert22(values.ValuePosition11 + values.ValuePosition8);
  values.ValuePosition30 = convert22(values.ValuePosition12 * 2);
  values.ValuePositionA = convert22(values.ValuePosition1 + values.ValuePosition4);
  values.ValuePositionB = convert22(values.ValuePosition2 + values.ValuePosition4);
  values.ValuePositionC = convert22(values.ValuePosition2 + values.ValuePosition5);
  values.ValuePositionD = convert22(values.ValuePosition3 + values.ValuePosition5);
  values.ValuePositionE = convert22(values.ValuePosition4 + values.ValuePosition6);
  values.ValuePositionF = convert22(values.ValuePosition5 + values.ValuePosition6);
  values.ValuePositionH = convert22(values.ValuePositionA + values.ValuePositionE);
  values.NegativeIndividPortrait1 = convert22(convertToArab(individPortraitValues1.ValuePositionA) + convertToArab(individPortraitValues1.ValuePosition12));
  values.NegativeIndividPortrait2 = convert22(convertToArab(individPortraitValues2.ValuePositionA) + convertToArab(individPortraitValues2.ValuePosition12));
  values.NegativeTwoPortrait = convert22(values.ValuePositionA + values.ValuePosition12);

  for (var k in values) {
    values[k] = convertToRoman(values[k]);
  }
  return values;
}

export function calcCommonPortrait(individPortraitValues1, individPortraitValues2) {
  var values = {};
  values.ValuePosition1 = convert22(convertToArab(individPortraitValues1.ValuePosition1) + convertToArab(individPortraitValues2.ValuePosition1));
  values.ValuePosition2 = convert22(convertToArab(individPortraitValues1.ValuePosition2) + convertToArab(individPortraitValues2.ValuePosition2));
  values.ValuePosition3 = convert22(convertToArab(individPortraitValues1.ValuePosition3) + convertToArab(individPortraitValues2.ValuePosition3));
  values.ValuePosition4 = convert22(convertToArab(individPortraitValues1.ValuePosition4) + convertToArab(individPortraitValues2.ValuePosition4));
  values.ValuePosition5 = convert22(convertToArab(individPortraitValues1.ValuePosition5) + convertToArab(individPortraitValues2.ValuePosition5));
  values.ValuePosition6 = convert22(convertToArab(individPortraitValues1.ValuePosition6) + convertToArab(individPortraitValues2.ValuePosition6));
  values.ValuePosition7 = convert22(convertToArab(individPortraitValues1.ValuePosition7) + convertToArab(individPortraitValues2.ValuePosition7));
  values.ValuePosition8 = convert22(convertToArab(individPortraitValues1.ValuePosition8) + convertToArab(individPortraitValues2.ValuePosition8));
  values.ValuePosition12 = convert22(convertToArab(individPortraitValues1.ValuePosition12) + convertToArab(individPortraitValues2.ValuePosition12));
  values.ValuePosition13 = convert22(convertToArab(individPortraitValues1.ValuePosition13) + convertToArab(individPortraitValues2.ValuePosition13));
  values.ValuePosition14 = convert22(convertToArab(individPortraitValues1.ValuePosition14) + convertToArab(individPortraitValues2.ValuePosition14));
  values.ValuePosition16 = convert22(convertToArab(individPortraitValues1.ValuePosition16) + convertToArab(individPortraitValues2.ValuePosition16));
  values.ValuePosition9 = convert22(convertToArab(individPortraitValues1.ValuePosition9) + convertToArab(individPortraitValues2.ValuePosition9));
  values.ValuePosition10 = convert22(convertToArab(individPortraitValues1.ValuePosition10) + convertToArab(individPortraitValues2.ValuePosition10));
  values.ValuePosition11 = convert22(convertToArab(individPortraitValues1.ValuePosition11) + convertToArab(individPortraitValues2.ValuePosition11));
  values.ValuePosition15 = convert22(convertToArab(individPortraitValues1.ValuePosition15) + convertToArab(individPortraitValues2.ValuePosition15));
  values.ValuePosition17 = convert22(convertToArab(individPortraitValues1.ValuePosition17) + convertToArab(individPortraitValues2.ValuePosition17));
  values.ValuePosition18 = convert22(convertToArab(individPortraitValues1.ValuePosition18) + convertToArab(individPortraitValues2.ValuePosition18));
  values.ValuePosition30 = convert22(convertToArab(individPortraitValues1.ValuePosition30) + convertToArab(individPortraitValues2.ValuePosition30));
  values.ValuePositionA = convert22(convertToArab(individPortraitValues1.ValuePositionA) + convertToArab(individPortraitValues2.ValuePositionA));
  values.ValuePositionB = convert22(convertToArab(individPortraitValues1.ValuePositionB) + convertToArab(individPortraitValues2.ValuePositionB));
  values.ValuePositionC = convert22(convertToArab(individPortraitValues1.ValuePositionC) + convertToArab(individPortraitValues2.ValuePositionC));
  values.ValuePositionD = convert22(convertToArab(individPortraitValues1.ValuePositionD) + convertToArab(individPortraitValues2.ValuePositionD));
  values.ValuePositionE = convert22(convertToArab(individPortraitValues1.ValuePositionE) + convertToArab(individPortraitValues2.ValuePositionE));
  values.ValuePositionF = convert22(convertToArab(individPortraitValues1.ValuePositionF) + convertToArab(individPortraitValues2.ValuePositionF));
  values.ValuePositionH = convert22(convertToArab(individPortraitValues1.ValuePositionH) + convertToArab(individPortraitValues2.ValuePositionH));
  values.NegativeIndividPortrait1 = convert22(convertToArab(individPortraitValues1.ValuePositionA) + convertToArab(individPortraitValues1.ValuePosition12));
  values.NegativeIndividPortrait2 = convert22(convertToArab(individPortraitValues2.ValuePositionA) + convertToArab(individPortraitValues2.ValuePosition12));
  values.NegativeTwoPortrait = convert22(values.ValuePositionA + values.ValuePosition12);

  for (var k in values) {
    values[k] = convertToRoman(values[k]);
  }
  return values;
}

export function getDescPositionForIndividPortrait(numberPosition) {
  switch (numberPosition) {
    case "1":
      return "Предназначение. Внутренний ребёнок. «Багаж, с которым пришли».";
    case "2":
      return "Школа психологического взросления. Внутренний взрослый.";
    case "3":
      return "Экзаменатор, к чему приходим.";
    case "4":
      return "Бессознательное. Страхи и комплексы.";
    case "5":
      return "Сознательное. Устремления. Социальная маска.";
    case "6":
      return "Сверхсознание. Жизненный сценарий.";
    case "7":
      return "Миссия и ЛжеМиссия.";
    case "8":
      return "Инструмент Миссии. Дар.";
    case "9":
      return "Позиционирование себя. Генетический опыт и качества.";
    case "10":
      return "Генетические устремления, что может помешать.";
    case "11":
      return "Генетическая Миссия. Недоработки.";
    case "12":
      return "Зона психологической гармонии.";
    case "13":
      return "Зона психологической дисгармонии.";
    case "14":
      return "Репутация. Как нас видят другие.";
    case "15":
      return "Навыки и способ открытия Ген.Миссии";
    case "16":
      return "«Грабли». В чём застряли.";
    case "17":
      return "Генетический Комфорт. Отрывает тему жизни.";
    case "18":
      return "Отрывает Миссию. Помогает понять как.";
    case "30":
      return "Зона психологической дисгармонии.";
    case "A":
      return "Подавленная Энергия. Закрытые таланты и желания.";
    case "B":
      return "Психологическая травма из детства. «Оковы»";
    case "C":
      return "Потребность души. В чём можем преуспеть.";
    case "D":
      return "«Фасад». Кем хотим казаться.";
    case "E":
      return "Бессознательный талант. Защита и страх.";
    case "F":
      return "Неосознаваемый потенциал, который сами отвергаем.";
    case "H":
      return "Деструктивное поведение. Чем себя разрушаем.";
    default:
      return "Описание отсутствует, но оно скоро тут появится.";
  }
}

/**
 * Соответствия арканов буквам русского и английского алфавита
 * @param {*} char
 * @returns
 */
export function getPositionForKhrzanowska(char) {
  char = char.toUpperCase();
  if (char == "А") return "XXII";
  if (char == "Б") return "I";
  if (char == "В") return "II";
  if (char == "Г") return "III";
  if (char == "Д") return "IV";
  if (char == "Е") return "V";
  if (char == "Ё") return "V";
  if (char == "Ж") return "VI";
  if (char == "З") return "VI";
  if (char == "И") return "VII";
  if (char == "Й") return "VII";
  if (char == "К") return "VIII";
  if (char == "Л") return "IX";
  if (char == "М") return "X";
  if (char == "Н") return "XI";
  if (char == "О") return "XII";
  if (char == "П") return "XIII";
  if (char == "Р") return "XIV";
  if (char == "С") return "XV";
  if (char == "Т") return "XVI";
  if (char == "У") return "XVII";
  if (char == "Ф") return "XVIII";
  if (char == "Х") return "XVIII";
  if (char == "Ц") return "XV";
  if (char == "Ч") return "III";
  if (char == "Ш") return "XIX";
  if (char == "Щ") return "XIX";
  if (char == "Ы") return "XX";
  if (char == "Ь") return "XX";
  if (char == "Ъ") return "XX";
  if (char == "Э") return "V";
  if (char == "Ю") return "XXI";
  if (char == "Я") return "XXI";
  if (char == "A") return "XXII";
  if (char == "B") return "I";
  if (char == "C") return "II";
  if (char == "D") return "III";
  if (char == "E") return "IV";
  if (char == "F") return "V";
  if (char == "G") return "VI";
  if (char == "H") return "VII";
  if (char == "I") return "VIII";
  if (char == "J") return "VIII";
  if (char == "K") return "X";
  if (char == "L") return "XI";
  if (char == "M") return "XII";
  if (char == "N") return "XIII";
  if (char == "O") return "XIV";
  if (char == "P") return "XV";
  if (char == "Q") return "X";
  if (char == "R") return "XVI";
  if (char == "S") return "XVII";
  if (char == "T") return "XVIII";
  if (char == "U") return "XIV";
  if (char == "V") return "XIX";
  if (char == "W") return "XIX";
  if (char == "X") return "XX";
  if (char == "Y") return "IX";
  if (char == "Z") return "XXI";
}
export function getMainArcaneForKhrzanowska(name) {
  var sum = 0;

  name = name.toUpperCase();
  name.split("").forEach((char) => {
    var pos = getPositionForKhrzanowska(char);
    var number = convertToArab(pos);
    sum += parseInt(number);
  });
  sum = convert22(sum);
  return convertToRoman(sum);
}

export function getSumArchetype(nameArchetype, values) {
  var valueSumSupM = 0;
  var valueSumSupZh = 0;
  var valueSumSupN = 0;
  if (values.ValueSupPosition1 == "м") valueSumSupM++;
  if (values.ValueSupPosition1 == "ж") valueSumSupZh++;
  if (values.ValueSupPosition1 == "н") valueSumSupN++;
  if (values.ValueSupPosition2 == "м") valueSumSupM++;
  if (values.ValueSupPosition2 == "ж") valueSumSupZh++;
  if (values.ValueSupPosition2 == "н") valueSumSupN++;
  if (values.ValueSupPosition3 == "м") valueSumSupM++;
  if (values.ValueSupPosition3 == "ж") valueSumSupZh++;
  if (values.ValueSupPosition3 == "н") valueSumSupN++;
  if (values.ValueSupPosition4 == "м") valueSumSupM++;
  if (values.ValueSupPosition4 == "ж") valueSumSupZh++;
  if (values.ValueSupPosition4 == "н") valueSumSupN++;
  if (values.ValueSupPosition5 == "м") valueSumSupM++;
  if (values.ValueSupPosition5 == "ж") valueSumSupZh++;
  if (values.ValueSupPosition5 == "н") valueSumSupN++;
  if (values.ValueSupPosition6 == "м") valueSumSupM++;
  if (values.ValueSupPosition6 == "ж") valueSumSupZh++;
  if (values.ValueSupPosition6 == "н") valueSumSupN++;
  if (values.ValueSupPosition7 == "м") valueSumSupM++;
  if (values.ValueSupPosition7 == "ж") valueSumSupZh++;
  if (values.ValueSupPosition7 == "н") valueSumSupN++;
  if (values.ValueSupPosition8 == "м") valueSumSupM++;
  if (values.ValueSupPosition8 == "ж") valueSumSupZh++;
  if (values.ValueSupPosition8 == "н") valueSumSupN++;
  if (nameArchetype == "м") return valueSumSupM;
  if (nameArchetype == "ж") return valueSumSupZh;
  if (nameArchetype == "н") return valueSumSupN;
  return 0;
}

export function getPeriod(dt) {
  var values = {};
  var dtString = dt + "";
  var periodEndCircle1 = 0;
  for (let i = 0; i < dtString.length; i++) {
    var parsed = parseInt(dtString[i]);
    if (!isNaN(parsed)) {
      periodEndCircle1 += parsed;
    }
  }
  var periodStartCircle2 = periodEndCircle1 + 1;
  var periodEndCircle2 = periodStartCircle2 + 8;
  var periodStartCircle3 = periodEndCircle2 + 1;

  values.PeriodEndCircle1 = periodEndCircle1;
  values.PeriodStartCircle2 = periodStartCircle2;
  values.PeriodEndCircle2 = periodEndCircle2;
  values.PeriodStartCircle3 = periodStartCircle3;
  return values;
}
