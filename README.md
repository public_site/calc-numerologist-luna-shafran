## Пакеты

- npx create-react-app calc-numerologist-luna-shafran.site
- npm i --save react-router-dom
- npm i --save bootstrap
- npm i --save react-bootstrap
- npm i --save react-input-mask
- npm i --save @fortawesome/fontawesome-svg-core
- npm i --save @fortawesome/free-solid-svg-icons
- npm i --save @fortawesome/react-fontawesome
- npm i --save sass
- npm i --save react-icons
- npm i --save react-redux
- npm i --save react
- npm i --save redux-thunk
- npm i --save html-to-image
- npm i --save jspdf

## Сборка

### npm i --save react-uuid

### npm run build

### Подготовка к загрузки на GitLab Page:

> Замена в build/\*.html
>
> > ="/  
> > ="

> Замена в build/static/js/\*.js
>
> > n.p+"static/media/  
> > "static/media/

> Замена в build/static/css/\*.css
>
> > url(/static/media/  
> > url(../media/
